import React from 'react'
import Home from './pages/landing/Home'
import { Routes, Route } from 'react-router-dom'
import './style.css'
import './responsive.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Screens } from './constant/routes'
import About from './pages/landing/About'
import ContactUs from './pages/landing/ContactUs'
import Login from './pages/auth/Login'
import SignUp from './pages/auth/SingUp'
import SetupProfile from './pages/auth/SetupProfile'
import Connects from './pages/auth/Connects'
import Location from './pages/auth/Location'
import Interests from './pages/auth/Interests'
import Forgot from './pages/auth/Forgot'
import OtpCode from './pages/auth/Otp'
import CreatePassword from './pages/auth/CreatePassword'
import KindHome from './pages/kindAdmin/Home'
import BlossomKind from './pages/kindAdmin/BlossomKind'
import BlossomDetail from './pages/kindAdmin/BlossomDetail'
import PaymentMethod from './pages/kindAdmin/PaymentMethod'
import PaymentDetail from './pages/kindAdmin/PaymentDetail'
import AboutKind from './pages/kindAdmin/AboutKind'
import AboutProfile from './pages/kindAdmin/AboutProfile'
import CreatePost from './pages/kindAdmin/CreatePost'
import Overview from './pages/kindAdmin/Overview'
import InsightKind from './pages/kindAdmin/InsightKind'
import BlossomHome from './pages/blossomConnect/ConnectHome'
import AboutConnect from './pages/blossomConnect/AboutConnect'
import Schedule from './pages/blossomConnect/Schedule'
import ProfileAbout from './pages/blossomConnect/ProfileAbout'
import AboutDetail from './pages/blossomConnect/AboutDetail'
import CreateService from './pages/blossomConnect/CreateService'
import HomeView from './pages/blossomView/HomeView'
import BlossomViewAbout from './pages/blossomView/BlossomViewAbout'
import ViewProfile from './pages/blossomView/ViewProfile'
import ViewCreatePost from './pages/blossomView/ViewCreatePost'
import PostOverView from './pages/blossomView/PostOverView'
import EditMembership from './pages/blossomView/EditMembership'
import MartHome from './pages/blossomMart/MartHome'
import ProductDetail from './pages/blossomMart/ProductDetail'
import AboutMart from './pages/blossomMart/AboutMart'
import ProductReview from './pages/blossomMart/ProductReview'

function App() {
  return (
    <>
      <Routes>
        {/* Landing Screens */}
        <Route path={Screens.Home} element={<Home />} />
        <Route path={Screens.About} element={<About />} />
        <Route path={Screens.contactUs} element={<ContactUs />} />
        {/* Auth Screens */}
        <Route path={Screens.login} element={<Login />} />
        <Route path={Screens.signUp} element={<SignUp />} />
        <Route path={Screens.setupProfile} element={<SetupProfile />} />
        <Route path={Screens.connects} element={<Connects />} />
        <Route path={Screens.locations} element={<Location />} />
        <Route path={Screens.interests} element={<Interests />} />
        <Route path={Screens.forgot} element={<Forgot />} />
        <Route path={Screens.enterOtp} element={<OtpCode />} />
        <Route path={Screens.createPassword} element={<CreatePassword />} />

        {/* Kind Landing */}
        <Route path={Screens.kindHome} element={<KindHome />} />
        <Route path={Screens.blossomKind} element={<BlossomKind />} />
        <Route path={Screens.blossomDetail} element={<BlossomDetail />} />
        <Route path={Screens.paymentMathod} element={<PaymentMethod />} />
        <Route path={Screens.paymentDetail} element={<PaymentDetail />} />
        <Route path={Screens.blossomAbout} element={<AboutKind />} />
        <Route path={Screens.aboutProfile} element={<AboutProfile />} />
        <Route path={Screens.createPost} element={<CreatePost />} />
        <Route path={Screens.overView} element={<Overview />} />
        <Route path={Screens.insightKind} element={<InsightKind />} />

        {/* Blossom Home */}
        <Route path={Screens.blossomHome} element={<BlossomHome />} />
        <Route path={Screens.aboutConnect} element={<AboutConnect />} />
        <Route path={Screens.connectSchedule} element={<Schedule />} />
        <Route path={Screens.profileAbout} element={<ProfileAbout />} />
        <Route path={Screens.aboutDetail} element={<AboutDetail />} />
        <Route path={Screens.createService} element={<CreateService />} />

        {/* Blossom View */}

        <Route path={Screens.blossomView} element={<HomeView />} />
        <Route path={Screens.blossomViewAbout} element={<BlossomViewAbout />} />
        <Route path={Screens.blossomViewProfile} element={<ViewProfile />} />
        <Route path={Screens.viewCreatePost} element={<ViewCreatePost />} />
        <Route path={Screens.postOverView} element={<PostOverView />} />
        <Route path={Screens.editMembership} element={<EditMembership />} />

        {/* Mart Blossom */}
        <Route path={Screens.martHome} element={<MartHome />} />
        <Route path={Screens.productDetail} element={<ProductDetail />} />
        <Route path={Screens.aboutMart} element={<AboutMart />} />
        <Route path={Screens.productReview} element={<ProductReview />} />
      </Routes>
    </>
  );

}

export default App
