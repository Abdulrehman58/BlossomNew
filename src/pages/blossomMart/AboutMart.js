import React, { useState } from "react";
import { Button, Card, Col, Form, Modal, Row, Tab, Tabs } from "react-bootstrap";
import Images from "../../constant/images";
import { blossomCard, feedbackReviews, historyListing, insightsListing, notificationList, productCardMart, textBlossom, viewAboutCard, viewHomeCard } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomViewHeader from "../../components/header/BlossomView";
import ChartBar from "../../commonComponents/chartBar";
import BlossomMartHeader from "../../components/header/BlossomMart";


function AboutMart() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomMartHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <Tabs
                            defaultActiveKey="service"
                            id="uncontrolled-tab-example"
                            className="mb-3 admin-blossom-tabs-profile"
                        >
                            <Tab eventKey="service" title="Service" className="admin-blossom-tab-link">
                                <div className="mt-4 blossom-view-group-container">
                                    <Row className="">
                                        {
                                            productCardMart.map((item, index) => {
                                                return (
                                                    <>
                                                        <Col lg={{ span: 4 }} md={{ span: 6 }} sm={{ span: 6 }}>
                                                            <Card className="user-mart-product-card" >
                                                                <div className="user-mart-product-logo" onClick={() => navigate(Screens.productDetail)}>
                                                                    <img src={item.image} className="img-fluid" />
                                                                </div>
                                                                <div className="user-mart-product-price mt-3">
                                                                    <h6 className="user-mart-item-price">
                                                                        <span className="user-mart-review-mark">
                                                                            <img src={item.active} className="user-mart-review-active" />
                                                                        </span>
                                                                        {item.title}
                                                                    </h6>
                                                                    <h6 className="user-mart-item-price">
                                                                        {item.price}
                                                                    </h6>
                                                                </div>
                                                                <div className="mt-3">
                                                                    <span className="user-mart-review-mark" onClick={()=>navigate(Screens.productReview)}>
                                                                        <img src={item.star} className="user-mart-review-star" />
                                                                        {item.review} <span className="theme-color-text">{item.count}</span>
                                                                    </span>
                                                                    <p className="user-card-paragraph-text">
                                                                        {item.text}
                                                                    </p>
                                                                </div>
                                                            </Card>
                                                        </Col>
                                                    </>
                                                );
                                            })
                                        }

                                    </Row>
                                </div>
                            </Tab>
                            <Tab eventKey="about" title="About" className="admin-blossom-tab-link">
                                <div className="admin-dashboard-portfolio-kind">
                                    <div className="create-account-admin-profile pt-2">
                                        <span className="user-account-logo-bar user-account-logo-bar-dashboard">
                                            <img src={Images.UserLogo} className="img-fluid" />
                                        </span>
                                        <div className="user-account-username user-account-title-dashboard">
                                            <h3 className="user-account-title user-account-title-username">
                                                Saline Durene
                                            </h3>
                                        </div>
                                    </div>
                                    <div className="mt-4 blossom-view-group-container">

                                        {
                                            blossomCard.map((item, index) => {
                                                return (
                                                    <>
                                                        <div className="blossom-view-content-card" onClick={() => navigate(item.link)}>
                                                            <div className="create-account-admin-profile">
                                                                <span className="user-account-logo-bar user-account-logo-bar-top user-account-logo-bar-admin">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username ">
                                                                    <h5 className="user-account-title user-account-title-white">
                                                                        {item.title}
                                                                    </h5>
                                                                    <h6 className="user-account-description-kind user-account-title-white">
                                                                        {item.text}
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <span className="user-account-logo-bar user-account-logo-bar-top user-account-logo-bar-admin">
                                                                <img src={item.arrow} className="img-fluid" />
                                                            </span>
                                                        </div>
                                                    </>
                                                )
                                            })
                                        }
                                        <div className="blossom-view-content-card-portfolio">
                                            <div className="blossom-view-content-card-flex ">
                                                <div className="create-account-admin-profile px-0">
                                                    <h3 className="user-account-title user-account-title-username">
                                                        Jane Doe | Graphic Designer
                                                    </h3>
                                                </div>
                                                <h5 className="heading-card-content-admin">
                                                    edit
                                                </h5>
                                            </div>
                                            <div className="mt-2">
                                                <p className="">
                                                    {
                                                        textBlossom.map((item, index) => {
                                                            return (
                                                                <>
                                                                    {item.text}
                                                                    <br />
                                                                    <br />
                                                                    {item.text2}
                                                                    <br />
                                                                    <br />
                                                                    {item.text3}
                                                                </>
                                                            )
                                                        })
                                                    }

                                                </p>
                                            </div>
                                        </div>
                                    </div >
                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
export default AboutMart;