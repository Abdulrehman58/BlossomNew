import React from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import Images from "../../constant/images";
import { notificationList } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomMartHeader from "../../components/header/BlossomMart";


function ProductDetail() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomMartHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-about-kind-heading">
                            <h5 className="user-account-title-theme text-uppercase">
                                product details
                            </h5>
                        </div>
                        <div className="contact-form-content-container product-detail-content-container">
                            <div className="mb-4 padding-10 mt-4 max-width-90">
                                <img src={Images.MartDetail} className="img-fluid img-fluid-vector-card-view img-fluid-vector-card-view-profile" />
                                <div className="admin-dashboard-product-detail">
                                    <h5 className="user-product-title-detail">
                                        Logo design
                                    </h5>
                                    <p className="text-featured-description">
                                        Digital
                                    </p>
                                </div>
                                <label className="user-profile-label-form mb-2 mt-4">
                                    <b>price: $25</b>
                                </label>
                                <p className="view-user-comments-count text-capitalize">
                                    and publish content more effectively. This could include features like advanced editing tools, analytics dashboards, and content scheduling options.
                                    <br /><br />
                                    and publish content more effectively. This could include features like advanced editing tools, analytics dashboards, and content scheduling options.
                                </p>

                                <div className="create-account-admin-profile px-0 py-0 mt-4"
                                >
                                    <span className="user-featured-logo mt-0">
                                        <img src={Images.CardUser} className="img-fluid" />
                                    </span>
                                    <div className="user-account-username ">
                                        <h6 className="text-user-featured-title text-left">
                                            JANE DOE
                                        </h6>
                                        <p className="product-detail-description text-left">
                                            go to seller profile
                                        </p>
                                    </div>
                                </div>
                                <div className="mt-4 user-donation-detail-form">
                                    <InputGroup className="mb-3 user-donation-detail-group product-donation-detail-group-container">
                                        <Form.Control
                                            placeholder="Enter coupon code"
                                            aria-label="Recipient's username"
                                            aria-describedby="basic-addon2"
                                            className="user-donation-detail-input"
                                        />
                                        <Button variant="outline-secondary" className="user-donation-detail-btn-primary product-donation-detail-btn-primary"
                                            onClick={() => navigate(Screens.paymentMathod)}>
                                            purchase
                                        </Button>
                                    </InputGroup>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
export default ProductDetail;