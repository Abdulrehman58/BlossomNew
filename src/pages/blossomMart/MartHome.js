import React, { useState } from "react";
import Images from "../../constant/images";
import { featureCard, featuredDetailCard, notificationList, productCardMart, viewHomeCard } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import BlossomKindHeader from "../../components/header/BlossomKind";
import BlossomViewHeader from "../../components/header/BlossomView";
import { Button, Card, Col, Modal, Row } from "react-bootstrap";
import { Screens } from "../../constant/routes";
import BlossomMartHeader from "../../components/header/BlossomMart";


function MartHome() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomMartHeader />
                    <div className="admin-dashboard-portfolio-kind">

                        <div className="admin-dashboard-portfolio-kind">
                            <h5 className="user-account-title-theme">
                                featured users
                            </h5>
                            <div className="mt-4 blossom-view-group-container">
                                <ul className="px-0 mb-2">
                                    {
                                        featureCard.map((item, index) => {
                                            return (
                                                <>
                                                    <li className="featured-user-list" onClick={() => navigate(Screens.aboutMart)}>
                                                        <span className="user-featured-logo">
                                                            <img src={item.image} className="img-fluid" />
                                                        </span>
                                                        <h6 className="text-user-featured-title">
                                                            {item.title}
                                                        </h6>
                                                        <p className="text-user-featured-description">
                                                            {item.description}
                                                        </p>
                                                    </li>
                                                </>
                                            )
                                        })
                                    }

                                </ul>
                            </div>
                        </div>
                        <div className="mt-4 blossom-view-group-container">
                            <Row className="">
                                {
                                    productCardMart.map((item, index) => {
                                        return (
                                            <>
                                                <Col lg={{ span: 4 }} md={{ span: 6 }} sm={{ span: 6 }}>
                                                    <Card className="user-mart-product-card" >
                                                        <div className="user-mart-product-logo" onClick={()=>navigate(Screens.productDetail)}>
                                                            <img src={item.image} className="img-fluid" />
                                                        </div>
                                                        <div className="user-mart-product-price mt-3">
                                                            <h6 className="user-mart-item-price">
                                                                <span className="user-mart-review-mark">
                                                                    <img src={item.active} className="user-mart-review-active" />
                                                                </span>
                                                                {item.title}
                                                            </h6>
                                                            <h6 className="user-mart-item-price">
                                                                {item.price}
                                                            </h6>
                                                        </div>
                                                        <div className="mt-3">
                                                            <span className="user-mart-review-mark" onClick={()=>navigate(Screens.productReview)}>
                                                                <img src={item.star} className="user-mart-review-star" />
                                                                {item.review} <span className="theme-color-text">{item.count}</span>
                                                            </span>
                                                            <p className="user-card-paragraph-text">
                                                                {item.text}
                                                            </p>
                                                        </div>
                                                    </Card>
                                                </Col>
                                            </>
                                        );
                                    })
                                }

                            </Row>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center mt-4">
                        <p className="user-text-title-left text-capitalize">
                            $5 for this content, you
                            <br />
                            want to view
                        </p>
                        <Button variant="outline-secondary" className="user-donation-detail-btn-primary detail-btn-primary-pay mt-4 text-capitalize"
                            onClick={() => navigate(Screens.paymentMathod)}  >
                            pay
                        </Button>
                    </div>
                </Modal.Body>

            </Modal>
        </>
    );
}
export default MartHome;