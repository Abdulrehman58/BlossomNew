import React from "react";
import Images from "../../constant/images";
import { notificationList, textBlossom } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomMartHeader from "../../components/header/BlossomMart";
import { RatingStars } from "../../commonComponents/ratingStar";
import { Button, Form } from "react-bootstrap";


function ProductReview() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomMartHeader />
                    <div className="admin-dashboard-portfolio-kind max-width-90">
                        <div className="blossom-view-content-card-portfolio ">
                            <h5 className=" account-title-review-product">
                                leave a review
                            </h5>
                            <div className="mt-4 user-review-product-theme-card">
                                <span className="user-review-product-theme-logo">
                                    <img src={Images.King} className="img-fluid" />
                                </span>
                                <div className="user-product-right-content">
                                    <span className="user-mart-review-mark">
                                        <img src={Images.StarYellow} className="user-mart-review-star" />
                                        4.7  <span className="theme-color-text">(12)</span>
                                    </span>
                                    <h6 className="user-mart-item-price mt-3">
                                        Logo design
                                    </h6>
                                    <p className="user-card-paragraph-text mt-0">
                                        Do modern logo design
                                    </p>
                                    <h6 className="user-mart-item-price mt-2">
                                        price: $25
                                    </h6>
                                </div>
                            </div>
                            <h5 className="user-account-title-theme mt-5">
                                How is your order ?
                            </h5>
                            <p className="text-featured-description">
                                please give your rating also your review
                            </p>
                            <div className="mt-3">
                                <RatingStars />
                            </div>
                            <Form.Group className="mb-3 mt-4" controlId="exampleForm.ControlTextarea1">
                                <label className="user-profile-label-form mb-3">
                                    Review
                                </label>
                                <Form.Control className="form-control-text-content form-control-text-content-review" placeholder="Write Something" as="textarea" rows={4} />
                            </Form.Group>
                            <div className="text-left mt-5">
                                <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-service mt-1"
                                    onClick={() => navigate('')}>
                                    submit
                                </Button>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
export default ProductReview;