import React, { useState } from "react";
import { Form } from "react-bootstrap";
import Images from "../../constant/images";
import { featureCard, notificationList, servicesCards } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomConnectHeader from "../../components/header/BlossomConnect";


function BlossomHome() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                   <BlossomConnectHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-dashboard-portfolio-kind">
                            <h5 className="user-account-title-theme">
                                featured users
                            </h5>
                            <div className="mt-4 blossom-view-group-container">
                                <ul className="px-0 mb-3">
                                    {
                                        featureCard.map((item, index) => {
                                            return (
                                                <>
                                                    <li className="featured-user-list">
                                                        <span className="user-featured-logo" onClick={() => navigate(Screens.aboutConnect)}>
                                                            <img src={item.image} className="img-fluid" />
                                                        </span>
                                                        <h6 className="text-user-featured-title">
                                                            {item.title}
                                                        </h6>
                                                        <p className="text-user-featured-description">
                                                            {item.description}
                                                        </p>
                                                    </li>
                                                </>
                                            )
                                        })
                                    }

                                </ul>
                            </div>
                        </div>
                        <div className="services-tab-card-container">
                            {
                                servicesCards.map((item, index) => {
                                    return (
                                        <>
                                            <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer mb-4">
                                                <div className="mt-3">
                                                    <img src={item.attach} className="img-fluid" />
                                                </div>
                                                <div className="user-account-username mt-3">
                                                    <div className=" create-account-admin-services px-0 py-0 mb-3">
                                                        <h6 className="text-username-featured-title mt-2">
                                                            {item.title}
                                                        </h6>
                                                        <h6 className="text-username-featured-title mt-2">
                                                            {item.title2}
                                                        </h6>
                                                    </div>
                                                    <p className="text-featured-description text-left">
                                                        {item.text}
                                                    </p>
                                                </div>
                                            </div>
                                        </>
                                    )
                                })
                            }

                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default BlossomHome;