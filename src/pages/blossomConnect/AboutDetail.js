import React from "react";
import { Button, Form } from "react-bootstrap";
import Images from "../../constant/images";
import { featuredDetailCard, notificationList } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomConnectHeader from "../../components/header/BlossomConnect";


function AboutDetail() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                   <BlossomConnectHeader />
                    <div className="admin-dashboard-portfolio-kind">

                        <div className="admin-dashboard-portfolio-kind">
                            <div className="services-tab-card-container">
                                <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer mb-4"
                                >
                                    <div className="mt-3">
                                        <img src={Images.Signals} className="img-fluid" />
                                    </div>
                                    <div className="user-account-username mt-3">
                                        <div className=" create-account-admin-services px-0 py-0 mb-3">
                                            <h6 className="text-username-featured-title mt-2">
                                                schedule a call
                                            </h6>
                                            <h6 className="text-username-featured-title mt-2">
                                                $5
                                            </h6>
                                        </div>
                                        <p className="text-featured-description text-left">
                                            Lorem ipsum dolor sit amet consectetur. Nibh quis enim quam quam. Mi gravida sed suspendisse.Signals
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div className="mt-5 user-donation-detail-form">

                                <p className="text-featured-description text-left max-width-90-text mt-4">
                                    Lorem ipsum dolor sit amet consectetur. Pellentesque duis faucibus leo diam leo neque amet enim cras. Vel sem sapien nec elementum. Sem amet porttitor condimentum vitae duis volutpat commodo neque natoque. Dis dui lacus id in. Facilisi egestas a elit sit quis dui tristique proin.<br /><br />
                                    Sed vitae feugiat diam nunc arcu risus. Dolor donec vulputate pulvinar sit at mi eget gravida. Et feugiat mattis gravida vel ultrices. Semper sit ut cursus vitae eget cursus arcu lectus. Molestie venenatis adipiscing vehicula vulputate. Mauris sodales lorem velit dui turpis arcu elementum. Semper sed accumsan scelerisque libero mi. Non vulputate rutrum lectus feugiat velit. Aliquam tempus fermentum a risus.
                                    <br /><br />
                                    t massa masspharetra sit. Cursus dictumst magna libero feugiat. A erat placerat eget imperdiet. Commodo id egestas dis id senectus. Id lorem habitasse diam purus aliquam. Aliquet posuere sollicitudin egestas suscipit amet. Orci convallis mauris in nunc dolor. At vel fermentum mauris fermentum.
                                                                    </p>
                                <div className="text-left mt-5">
                                    <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-overview mt-1"
                                        onClick={() => navigate(Screens.createService)}>
                                        edit
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default AboutDetail;