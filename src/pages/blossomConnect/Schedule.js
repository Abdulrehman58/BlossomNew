import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import Images from "../../constant/images";
import { featureCard, notificationList, servicesCards, timeSlots } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import CalendarComponent from "../../commonComponents/calendar";
import BlossomConnectHeader from "../../components/header/BlossomConnect";


function Schedule() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomConnectHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-about-kind-heading">
                            <h5 className="user-account-title-theme text-uppercase">
                                select date
                            </h5>
                        </div>
                        <div className="user-appointment-calendar mt-4">
                            <CalendarComponent />
                        </div>
                        <div className="admin-about-kind-heading mt-5">
                            <h5 className="user-account-title-theme text-uppercase">
                                select time
                            </h5>
                        </div>
                        <ul className="px-0 mt-3">
                            {
                                timeSlots.map((item, index) => {
                                    return (
                                        <>
                                            <li className="user-time-slots-availability">
                                                {item.time}
                                            </li>
                                        </>
                                    );
                                })
                            }
                        </ul>
                        <div className="text-left mt-5">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-overview mt-1"
                                onClick={() => navigate(Screens.paymentMathod)}>
                                book
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default Schedule;