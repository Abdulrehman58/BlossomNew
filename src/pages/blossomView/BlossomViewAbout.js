import React, { useState } from "react";
import { Button, Form, Modal, Tab, Tabs } from "react-bootstrap";
import Images from "../../constant/images";
import { blossomCard, featuredDetailCard, notificationList, servicesCards, textBlossom, viewAboutCard, viewHomeCard } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomViewHeader from "../../components/header/BlossomView";


function BlossomViewAbout() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomViewHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <Tabs
                            defaultActiveKey="home"
                            id="uncontrolled-tab-example"
                            className="mb-3 admin-blossom-tabs-profile"
                        >
                            <Tab eventKey="home" title="Home" className="admin-blossom-tab-link">
                                <div className="mt-4 blossom-view-group-container">

                                    {
                                        viewHomeCard.map((item, index) => {
                                            return (
                                                <>
                                                    <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer">
                                                        <div className="blossom-view-content-card-feature" >
                                                            <div className="create-account-admin-profile px-0 py-0"
                                                                onClick={() => navigate(item.link)}>
                                                                <span className="user-featured-logo mt-0">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username ">
                                                                    <h6 className="text-user-featured-title">
                                                                        {item.title}
                                                                    </h6>
                                                                    <p className="text-user-featured-description text-left">
                                                                        {item.text}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span className="user-account-logo-bar user-account-logo-bar-dots user-account-logo-bar-top user-account-logo-bar-admin">
                                                                    <img src={item.arrow} className="img-fluid img-fluid-account-user dropbtn" />
                                                                </span>
                                                                <div class="dropdown-content">
                                                                    <p>Share</p>
                                                                    <p>Report</p>
                                                                    <p>Block</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="mt-3 position-relative">
                                                            <img src={item.vector} className="img-fluid img-fluid-vector-card-view" />
                                                            <span className="button-view-modal-card" onClick={handleShow}>
                                                                view
                                                            </span>
                                                        </div>
                                                        <div className="text-center comments-user-content-card-container">
                                                            <ul className="px-0 mb-0 comments-like-container">
                                                                <li className="user-like-comments-logo">
                                                                    <span className="user-like-comments-text">
                                                                        <img src={Images.HeartBlue} className="user-like-comments-image" />
                                                                        like
                                                                    </span>
                                                                </li>
                                                                <li className="user-like-comments-logo">
                                                                    <span className="user-like-comments-text">
                                                                        <img src={Images.MessageDots} className="user-like-comments-image" />
                                                                        comments
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div className="mt-4 user-view-card-home-content">
                                                            <div className="view-user-notifications">
                                                                <span className="user-view-notification-container">
                                                                    <img src={item.alert1} className="img-fluid-alert" />
                                                                    <img src={item.alert2} className="img-fluid-alert img-fluid-alert-logo" />
                                                                    <img src={item.alert3} className="img-fluid-alert img-fluid-alert-logo" />
                                                                    <img src={item.alert4} className="img-fluid-alert img-fluid-alert-logo" />

                                                                </span>
                                                                <span className="user-view-list-likes">
                                                                    124 Likes
                                                                </span>
                                                            </div>
                                                            <div className="view-comments-description">
                                                                <p className="view-user-comments-count">
                                                                    {item.comments}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                            )
                                        })
                                    }
                                </div>
                            </Tab>
                            <Tab eventKey="membership" title="Membership" className="admin-blossom-tab-link">
                                <div className="services-tab-card-container">
                                    <div className="mt-4 blossom-view-group-container">

                                        {
                                            viewAboutCard.map((item, index) => {
                                                return (
                                                    <>
                                                        <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer">
                                                            <div className="create-account-admin-profile create-account-admin-profile-view px-0 py-0"
                                                            >
                                                                <span className="user-view-logo-content mt-0">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username user-account-username-view text-left">
                                                                    <div className="">
                                                                        <h6 className="text-user-view-title">
                                                                            {item.title}
                                                                        </h6>
                                                                        <p className="text-user-view-description text-left">
                                                                            {item.text}
                                                                        </p>
                                                                        <div className=" mt-3">
                                                                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-view mt-1"
                                                                            >
                                                                                join
                                                                            </Button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div className="user-account-username mt-4">
                                                                <p className="text-featured-description text-left">
                                                                    {item.paragraph}
                                                                </p>
                                                                <h6 className="text-username-featured-title text-username-featured-title-view mt-2">
                                                                    {item.bottomTitle}
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </>
                                                )
                                            })
                                        }
                                    </div>

                                </div>
                            </Tab>
                            <Tab eventKey="about" title="About" className="admin-blossom-tab-link">
                                <div className="admin-dashboard-portfolio-kind">
                                    <div className="create-account-admin-profile pt-2">
                                        <span className="user-account-logo-bar user-account-logo-bar-dashboard">
                                            <img src={Images.UserLogo} className="img-fluid" />
                                        </span>
                                        <div className="user-account-username user-account-title-dashboard">
                                            <h3 className="user-account-title user-account-title-username">
                                                Saline Durene
                                            </h3>
                                        </div>
                                    </div>
                                    <div className="mt-4 blossom-view-group-container">

                                        {
                                            blossomCard.map((item, index) => {
                                                return (
                                                    <>
                                                        <div className="blossom-view-content-card" onClick={() => navigate(item.link)}>
                                                            <div className="create-account-admin-profile">
                                                                <span className="user-account-logo-bar user-account-logo-bar-top user-account-logo-bar-admin">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username ">
                                                                    <h5 className="user-account-title user-account-title-white">
                                                                        {item.title}
                                                                    </h5>
                                                                    <h6 className="user-account-description-kind user-account-title-white">
                                                                        {item.text}
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <span className="user-account-logo-bar user-account-logo-bar-top user-account-logo-bar-admin">
                                                                <img src={item.arrow} className="img-fluid" />
                                                            </span>
                                                        </div>
                                                    </>
                                                )
                                            })
                                        }
                                        <div className="blossom-view-content-card-portfolio">
                                            <div className="blossom-view-content-card-flex ">
                                                <div className="create-account-admin-profile px-0">
                                                    <h3 className="user-account-title user-account-title-username">
                                                        Jane Doe | Graphic Designer
                                                    </h3>
                                                </div>
                                                <h5 className="heading-card-content-admin">
                                                    edit
                                                </h5>
                                            </div>
                                            <div className="mt-2">
                                                <p className="">
                                                    {
                                                        textBlossom.map((item, index) => {
                                                            return (
                                                                <>
                                                                    {item.text}
                                                                    <br />
                                                                    <br />
                                                                    {item.text2}
                                                                    <br />
                                                                    <br />
                                                                    {item.text3}
                                                                </>
                                                            )
                                                        })
                                                    }

                                                </p>
                                            </div>
                                        </div>
                                    </div >
                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center mt-4">
                        <p className="user-text-title-left text-capitalize">
                            $5 for this content, you
                            <br />
                            want to view
                        </p>
                        <Button variant="outline-secondary" className="user-donation-detail-btn-primary detail-btn-primary-pay mt-4 text-capitalize"
                            onClick={() => navigate(Screens.paymentMathod)}  >
                            pay
                        </Button>
                    </div>
                </Modal.Body>

            </Modal>
        </>
    );
}
export default BlossomViewAbout;