import React, { useState } from "react";
import { Button, Form, Modal, Tab, Tabs } from "react-bootstrap";
import Images from "../../constant/images";
import { blossomCard, feedbackReviews, historyListing, insightsListing, notificationList, textBlossom, viewAboutCard, viewHomeCard } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomViewHeader from "../../components/header/BlossomView";
import ChartBar from "../../commonComponents/chartBar";


function ViewProfile() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomViewHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <Tabs
                            defaultActiveKey="home"
                            id="uncontrolled-tab-example"
                            className="mb-3 admin-blossom-tabs-profile"
                        >
                            <Tab eventKey="home" title="Home" className="admin-blossom-tab-link">
                                <div className="mt-4 blossom-view-group-container">
                                    <div className="admin-about-kind-heading">
                                        <h5 className="user-account-title-theme text-capitalize">
                                            insights
                                        </h5>
                                        <h6 className="user-account-link-all">
                                            see all
                                        </h6>
                                    </div>
                                    <div className="mt-3 text-center blossom-view-amount-donations blossom-content-card-feature-continer">
                                        <h6 className="text-username-insight-title mt-2">
                                            view payment
                                        </h6>
                                        <h3 className="title-username-insight-update">
                                            $2390
                                        </h3>
                                    </div>
                                    <div className="admin-about-kind-heading mt-5">
                                        <h5 className="user-account-title-theme text-capitalize">
                                            latest posts
                                        </h5>
                                        <h6 className="user-account-link-all"
                                            onClick={() => navigate(Screens.viewCreatePost)}>
                                            create
                                        </h6>
                                    </div>
                                    {
                                        viewHomeCard.map((item, index) => {
                                            return (
                                                <>
                                                    <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer">
                                                        <div className="blossom-view-content-card-feature" >
                                                            <div className="create-account-admin-profile px-0 py-0"
                                                                onClick={() => navigate(item.link)}>
                                                                <span className="user-featured-logo mt-0">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username ">
                                                                    <h6 className="text-user-featured-title">
                                                                        {item.title}
                                                                    </h6>
                                                                    <p className="text-user-featured-description text-left">
                                                                        {item.text}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="dropdown">
                                                                <span className="user-account-logo-bar user-account-logo-bar-dots user-account-logo-bar-top user-account-logo-bar-admin">
                                                                    <img src={item.arrow} className="img-fluid img-fluid-account-user dropbtn" />
                                                                </span>
                                                                <div class="dropdown-content">
                                                                    <p>Share</p>
                                                                    <p>Report</p>
                                                                    <p>Block</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="mt-3 position-relative">
                                                            <img src={item.vector} className="img-fluid img-fluid-vector-card-view img-fluid-vector-card-view-profile" />
                                                        </div>
                                                        <div className="text-center comments-user-content-card-container">
                                                            <ul className="px-0 mb-0 comments-like-container">
                                                                <li className="user-like-comments-logo">
                                                                    <span className="user-like-comments-text">
                                                                        <img src={Images.HeartBlue} className="user-like-comments-image" />
                                                                        like
                                                                    </span>
                                                                </li>
                                                                <li className="user-like-comments-logo">
                                                                    <span className="user-like-comments-text">
                                                                        <img src={Images.MessageDots} className="user-like-comments-image" />
                                                                        comments
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div className="mt-4 user-view-card-home-content">
                                                            <div className="view-user-notifications">
                                                                <span className="user-view-notification-container">
                                                                    <img src={item.alert1} className="img-fluid-alert" />
                                                                    <img src={item.alert2} className="img-fluid-alert img-fluid-alert-logo" />
                                                                    <img src={item.alert3} className="img-fluid-alert img-fluid-alert-logo" />
                                                                    <img src={item.alert4} className="img-fluid-alert img-fluid-alert-logo" />

                                                                </span>
                                                                <span className="user-view-list-likes">
                                                                    124 Likes
                                                                </span>
                                                            </div>
                                                            <div className="view-comments-description">
                                                                <p className="view-user-comments-count">
                                                                    {item.comments}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                            )
                                        })
                                    }
                                </div>
                            </Tab>
                            <Tab eventKey="insight" title="Insight" className="admin-blossom-tab-link">
                                <div className="admin-dashboard-portfolio-kind">

                                    <div className="mt-3 text-center blossom-view-amount-donations blossom-content-card-feature-continer">
                                        <h6 className="text-username-insight-title mt-2">
                                            views payment

                                        </h6>
                                        <h3 className="title-username-insight-update">
                                            $2390
                                        </h3>
                                    </div>
                                    <div className="mt-3 text-center blossom-view-amount-donations blossom-content-card-feature-continer">
                                        <div className="user-chart-insight-container">
                                            <p className="user-text-title-left">
                                                Post
                                            </p>
                                            <div className="user-content-logo-arrows">
                                                <span className="user-bar-chart-content">
                                                    <img src={Images.ArrowBg} className="img-fluid" />
                                                </span>
                                                <span className="user-bar-chart-content">
                                                    <img src={Images.ArrowBgRight} className="img-fluid" />
                                                </span>
                                            </div>
                                        </div>
                                        <ChartBar />
                                    </div>
                                    <div className="admin-about-kind-heading mt-4">
                                        <h5 className="user-account-title-theme">
                                            history
                                        </h5>
                                        <h6 className="user-account-link-all">
                                            see all
                                        </h6>
                                    </div>
                                    <div className="mt-3 blossom-view-amount-donations blossom-content-card-feature-continer">
                                        <p className="view-user-comments-count">
                                            Nov,25,2030
                                        </p>
                                        <label className="user-profile-label-form user-profile-label-insights mb-2">
                                            Nature is Beautiful
                                        </label>
                                        <div className="user-profile-avator-portfolio">
                                            <img src={Images.cardVectorView} className="img-fluid" />
                                        </div>
                                        <ul className="px-0">
                                            {
                                                insightsListing.map((item, index) => {
                                                    return (
                                                        <>
                                                            <li className="user-profile-insights-listing">
                                                                {item.label}
                                                            </li>
                                                        </>
                                                    );
                                                })
                                            }

                                        </ul>
                                    </div>
                                </div>
                            </Tab>
                            <Tab eventKey="membership" title="Membership" className="admin-blossom-tab-link">
                                <div className="services-tab-card-container">
                                    <div className="mt-4 blossom-view-group-container">

                                        {
                                            viewAboutCard.map((item, index) => {
                                                return (
                                                    <>
                                                        <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer">
                                                            <div className="create-account-admin-profile create-account-admin-profile-view px-0 py-0"
                                                            >
                                                                <span className="user-view-logo-content mt-0">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username user-account-username-view text-left">
                                                                    <div className="">
                                                                        <h6 className="text-user-view-title">
                                                                            {item.title}
                                                                        </h6>
                                                                        <p className="text-user-view-description text-left">
                                                                            {item.text}
                                                                        </p>
                                                                        <div className=" mt-3">
                                                                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-view mt-1"
                                                                                onClick={() => navigate(Screens.editMembership)} >
                                                                                edit
                                                                            </Button>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div className="user-account-username mt-4">
                                                                <p className="text-featured-description text-left">
                                                                    {item.paragraph}
                                                                </p>
                                                                <h6 className="text-username-featured-title text-username-featured-title-view mt-2">
                                                                    {item.bottomTitle}
                                                                </h6>
                                                            </div>
                                                        </div>
                                                    </>
                                                )
                                            })
                                        }
                                    </div>

                                </div>
                            </Tab>
                            <Tab eventKey="feedback" title="Feedback" className="admin-blossom-tab-link">
                                <div className="services-tab-card-container">
                                    <div className="admin-about-kind-heading max-width-100">
                                        <h5 className="user-account-title-theme">
                                            reviews
                                        </h5>
                                    </div>
                                    {
                                        feedbackReviews.map((item, index) => {
                                            return (
                                                <>
                                                    <div className="user-reviews-feedback-card">
                                                        <div className="create-account-admin-profile px-0 py-0"
                                                            onClick={() => navigate('')}>
                                                            <span className="user-featured-logo mt-0">
                                                                <img src={item.user} className="img-fluid" />
                                                            </span>
                                                            <div className="user-account-username text-left">
                                                                <h6 className="text-user-featured-title text-white">
                                                                    {item.title}
                                                                </h6>
                                                                <p className="text-user-featured-description text-left text-white">
                                                                    {item.description}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div className="mt-3">
                                                            <span className="user-reviews-star-content">
                                                                <img src={item.star} className="img-fluid" />
                                                            </span>
                                                        </div>
                                                        <p className="text-white mt-3">
                                                            {item.paragraph}
                                                        </p>
                                                    </div>
                                                </>
                                            )
                                        })
                                    }

                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center mt-4">
                        <p className="user-text-title-left text-capitalize">
                            $5 for this content, you
                            <br />
                            want to view
                        </p>
                        <Button variant="outline-secondary" className="user-donation-detail-btn-primary detail-btn-primary-pay mt-4 text-capitalize"
                            onClick={() => navigate(Screens.paymentMathod)}  >
                            pay
                        </Button>
                    </div>
                </Modal.Body>

            </Modal>
        </>
    );
}
export default ViewProfile;