import React from "react";
import { Button, Form } from "react-bootstrap";
import Images from "../../constant/images";
import { editMembershipProfile, imageProfilePost, notificationList } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import ImagePicker from "../../commonComponents/imagePicker";
import { Screens } from "../../constant/routes";
import TagsComponent from "../../commonComponents/tagsInput";
import BlossomKindHeader from "../../components/header/BlossomKind";


function EditMembership() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomKindHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-about-kind-heading">
                            <h5 className="user-account-title-theme text-uppercase">
                                Edit membership
                            </h5>
                        </div>
                        <div className="contact-form-content-container">
                            <Form className="form-control-contact-content form-control-contact-post mt-3">


                                <div className=" mb-4">

                                    {
                                        editMembershipProfile.map((item, index) => {
                                            return (
                                                <>
                                                    <ImagePicker labelContainer={item.labelContainer} spanImage={item.spanImage}
                                                        coverImage={item.coverImage} />
                                                </>
                                            );
                                        })
                                    }

                                </div>
                                <Form.Group className="form-control-group-container mb-4" controlId="formBasicEmail">
                                    <Form.Control className="form-control-input-content pl-0" type='text' placeholder='Title' />
                                </Form.Group>
                                <Form.Group className="form-control-group-container mb-4" controlId="formBasicText">
                                    <Form.Control className="form-control-input-content pl-0" type='text' placeholder='Price' />
                                </Form.Group>
                                <Form.Group className="mb-3 mt-4" controlId="exampleForm.ControlTextarea1">
                                    <Form.Control className="form-control-text-content" placeholder="Description" as="textarea" rows={5} />
                                </Form.Group>

                                <div className="text-right mt-5">
                                    <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-service mt-1"
                                        onClick={() => navigate(Screens.blossomViewProfile)}>
                                        Save
                                    </Button>
                                </div>

                            </Form>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default EditMembership;