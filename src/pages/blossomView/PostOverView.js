import React from "react";
import { Button, Form } from "react-bootstrap";
import Images from "../../constant/images";
import { imageProfilePost, notificationList } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import { useNavigate } from "react-router-dom";
import ImagePicker from "../../commonComponents/imagePicker";
import InputContact from "../../commonComponents";
import { Screens } from "../../constant/routes";
import TagsComponent from "../../commonComponents/tagsInput";
import BlossomKindHeader from "../../components/header/BlossomKind";


function PostOverView() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <BlossomKindHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-about-kind-heading">
                            <h5 className="user-account-title-theme text-uppercase">
                                Create Post
                            </h5>
                        </div>
                        <div className="contact-form-content-container">
                            <Form className="form-control-contact-content form-control-contact-post mt-3">

                                <div className=" mb-4">
                                    <label className="user-profile-label-form mb-3">
                                        Select Photo / Video <span className="error-input">
                                            *
                                        </span>
                                    </label>
                                    <div className="user-form-post-card-content mb-4 padding-10">
                                        <div className="">
                                            <img src={Images.cardVectorView} className="img-fluid img-fluid-vector-card-view img-fluid-vector-card-view-profile" />
                                        </div>
                                    </div>

                                </div>


                                <div className="user-form-post-card-content mb-4">
                                    <label className="user-profile-label-form mb-2">
                                        What’s in your mind <span className="error-input">
                                            *
                                        </span>
                                    </label>
                                    <p className="view-user-comments-count">
                                        and publish content more effectively. This could include features like advanced editing tools,
                                        analytics dashboards, and content scheduling options.
                                    </p>
                                    <div className="mt-4 mb-3">
                                        <label className="user-profile-label-form mb-2">
                                            Tags <span className="error-input">
                                                *
                                            </span>
                                        </label>
                                        <p className="view-user-comments-count">
                                            #Nature #ocean #Nature #ocean
                                        </p>
                                    </div>
                                </div>
                                <div className="user-form-post-card-content mb-4">
                                    <label className="user-profile-label-form mb-2">
                                        price
                                    </label>
                                    <p className="view-user-comments-count">
                                        $12
                                    </p>
                                </div>

                                <div className="text-right mt-5">
                                    <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-service mt-1"
                                        onClick={() => navigate(Screens.blossomViewProfile)}>
                                        post
                                    </Button>
                                </div>

                            </Form>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
export default PostOverView;