import React from "react";
import NavbarComponent from "../../components/navbar";
import { Container, Row, Col, Button } from "react-bootstrap";
import Images from "../../constant/images";
import { aboutBanner, aboutBannerTwo } from "../../constant/dummyData";
import EmotiveComponent from "../../components/emotiveComponent";
import Footer from "../../components/footer/Footer";


function About() {
    return (
        <>
            <section className="home-content-section">
                <NavbarComponent />
                <div className="banner-text-content">
                    <p className="parapraph-content-banner">
                        lorem ipsum dollar sit
                    </p>
                    <p className="parapraph-content-banner">
                        lorem ipsum dollar sit
                    </p>
                    <p className="parapraph-content-banner">
                        lorem ipsum dollar sit
                    </p>
                </div>
            </section>
            <section className="home-banner-content-section">
                <Container>
                    <Row>
                        <Col lg={{ span: 6 }}>
                            <div className="create-account-section">
                                <p className="user-account-text">
                                    <span className="users-account-profile">
                                        <img src={Images.Stars} />
                                    </span>
                                    Based on <b>1,204</b> Happy Reviews
                                    <span className="users-account-profile">
                                        <img src={Images.Users} />
                                    </span>
                                </p>
                                <h2 className="banner-home-title-head">
                                    About the <span className="text-color-theme">
                                        blossom generation</span>
                                </h2>
                                <p className="mb-2">
                                    Lorem ipsum dolor sit amet consectetur. Quis aenean risus facilisis ut scelerisque magnis orci a. Odio
                                </p>
                                <div className="user-button-container">
                                    <Button className='btn-primary-content-theme'>login to your account
                                        <span className="arrow-hand-logo">
                                            <img src={Images.ArrowHand} />
                                        </span>
                                    </Button>
                                    <p className="helping-info-text">
                                        <img src={Images.Lock} className="protect-lock-icon" /> your information save with us
                                    </p>
                                </div>
                            </div>
                        </Col>
                        <Col lg={{ span: 6 }}>
                            <div className="user-banner-vactor-section about-banner-vactor-section mb-5">
                                <div className="about-banner-vactor-span">
                                    <img className="img-fluid" src={Images.AboutVactor} />
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
            <section className="video-player-section">
                <Container>
                    <Row>
                        {
                            aboutBanner.map((item, index) => {
                                return (
                                    <>
                                        <Col lg={{ span: 6 }} className="mt-5 ">
                                            <div className="about-banner-vactor-span about-vactor-image-content">
                                                <img src={item.banner} className="img-fluid" />
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 6 }} className="mt-5 about-banner-vactor-column">
                                            <div className="about-banner-vactor-content">
                                                <h2 className="about-banner-subheading-right">
                                                    {item.heading}
                                                </h2>
                                                <p className="about-banner-subheading-paragraph">
                                                    {item.textOne}
                                                    <br /><br />
                                                    {item.textTwo}
                                                </p>
                                            </div>
                                        </Col>
                                    </>
                                );
                            })
                        }
                         {
                            aboutBannerTwo.map((item, index) => {
                                return (
                                    <>
                                       
                                        <Col lg={{ span: 6 }} className="mt-5 about-banner-vactor-column">
                                            <div className="about-banner-vactor-content">
                                                <h2 className="about-banner-subheading-right">
                                                    {item.heading}
                                                </h2>
                                                <p className="about-banner-subheading-paragraph">
                                                    {item.textOne}
                                                    <br /><br />
                                                    {item.textTwo}
                                                </p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 6 }} className="mt-5 text-right">
                                            <div className="about-banner-vactor-span about-vactor-image-content">
                                                <img src={item.banner} className="img-fluid" />
                                            </div>
                                        </Col>
                                    </>
                                );
                            })
                        }

                    </Row>
                </Container>
            </section>
            <section className="emotive-headline-benefits">
                <EmotiveComponent />
            </section>
            <section className="home-banner-content-section">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-capitilize">
                        <span className="text-color-theme">
                            Download Now
                        </span>
                    </h2>
                    <ul className="order-download-store">
                        <li className="order-store-listing">
                            <span className="store-app-manager">
                                <img src={Images.AppStore} />
                            </span>
                        </li>
                        <li className="order-store-listing">
                            <span className="store-app-manager">
                                <img src={Images.Playstore} />
                            </span>
                        </li>

                    </ul>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default About;