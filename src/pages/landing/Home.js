import React from "react";
import NavbarComponent from "../../components/navbar";
import { Container, Row, Col, Button } from "react-bootstrap";
import Images from "../../constant/images";
import { socialAccount, textString, userList, videoRecord } from "../../constant/dummyData";
import Player from "../../components/player";
import VideoComponent from "../../components/player";
import EmotiveComponent from "../../components/emotiveComponent";
import PlatformComponent from "../../components/platformComponent";
import Portfolio from "../../components/portfolioComponent";
import Footer from "../../components/footer/Footer";


function Home() {
    return (
        <>
            <section className="home-content-section">
                <NavbarComponent />
                <div className="banner-text-content">
                    <p className="parapraph-content-banner">
                        lorem ipsum dollar sit
                    </p>
                    <p className="parapraph-content-banner">
                        lorem ipsum dollar sit
                    </p>
                    <p className="parapraph-content-banner">
                        lorem ipsum dollar sit
                    </p>
                </div>
            </section>
            <section className="home-banner-content-section">
                <Container>
                    <Row>
                        <Col lg={{ span: 6 }}>
                            <div className="create-account-section">
                                <p className="user-account-text">
                                    <span className="users-account-profile">
                                        <img src={Images.Stars} />
                                    </span>
                                    Based on <b>1,204</b> Happy Reviews
                                    <span className="users-account-profile">
                                        <img src={Images.Users} />
                                    </span>
                                </p>
                                <h2 className="banner-home-title-head">
                                    create the account and unlock <span className="text-color-theme">
                                        ocean of actions</span> at blossom
                                    <span className="btn-light-theme-danger">
                                        <img src={Images.Timer} /> Hurry - Make your account
                                    </span>
                                </h2>
                                <ul className="px-0 mt-3 order-user-list-item">
                                    {
                                        userList.map((item, index) => {
                                            return (
                                                <>
                                                    <li className="user-item-list-check">
                                                        <span className="user-item-check-symbol">
                                                            <img src={item.check} /> </span>
                                                        {item.text}
                                                    </li>
                                                </>
                                            )
                                        })
                                    }

                                </ul>
                                <div className="user-button-container">
                                    <Button className='btn-primary-content-theme'>login to your account
                                        <span className="arrow-hand-logo">
                                            <img src={Images.ArrowHand} />
                                        </span>
                                    </Button>
                                    <p className="helping-info-text">
                                        <img src={Images.Lock} className="protect-lock-icon" /> your information save with us
                                    </p>
                                </div>
                                <div className="user-account-profile-view">
                                    <span className="user-account-profile-logo">
                                        <img src={Images.Account} />
                                    </span>
                                    <div className="user-account-description">
                                        <h5 className="heading-user-account">
                                            Lorem ipsum dolor sit amet consectetur. Lacus mattis.
                                        </h5>
                                        <p className="mb-2">
                                            “Lorem ipsum dolor sit amet consectetur. Quis aenean risus facilisis ut scelerisque magnis orci a. Odio”
                                        </p>
                                        <p className="mb-0">
                                            <b> Charles Simpson</b>
                                        </p>
                                    </div>
                                </div>
                                <ul className="px-0 user-social-account-container">
                                    {socialAccount.map((item, index) => {
                                        return (
                                            <>
                                                <li className="user-social-account-list">
                                                    <span className="user-social-account-logo">
                                                        <img src={item.logo} />
                                                    </span>
                                                </li>
                                            </>
                                        )
                                    })
                                    }

                                </ul>
                            </div>
                        </Col>
                        <Col lg={{ span: 6 }}>
                            <div className="user-banner-vactor-section mb-5">
                                <div className="">
                                    <img className="img-fluid" src={Images.Banner1} />
                                </div>
                                <span className="">
                                    <img className="img-fluid" src={Images.ArrowRight} />
                                </span>
                            </div>
                            <div className="user-banner-vactor-section mb-5">

                                <span className="">
                                    <img className="img-fluid" src={Images.ArrowLeft} />
                                </span>
                                <div className="">
                                    <img className="img-fluid" src={Images.Banner2} />
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
            <section className="video-player-section">
                <Container>
                    <Row>

                        {videoRecord.map((item, index) => {
                            return (
                                <>
                                    <Col lg={{ span: 3 }} md={{ span: 4 }} sm={{ span: 6 }}>
                                        <VideoComponent source={item.source} />
                                    </Col>
                                </>
                            );
                        })
                        }
                    </Row>
                </Container>
            </section>
            <section className="emotive-headline-product home-banner-content-section">
                <Container>
                    <Row>
                        <Col lg={{ span: 6 }}>
                            <div className="user-banner-vactor-section user-banner-product-section mb-5">
                                <div className="user-banner-content-logo">
                                    <img className="img-fluid" src={Images.Banner3} />
                                </div>
                            </div>
                        </Col>
                        <Col lg={{ span: 6 }}>
                            <div className="create-account-section">
                                <p className="user-account-text">
                                    <span className="users-account-profile">
                                        <img src={Images.Stars} />
                                    </span>
                                    Based on <b>1,204</b> Happy Reviews
                                    <span className="users-account-profile">
                                        <img src={Images.Users} />
                                    </span>
                                </p>
                                <h2 className="banner-home-title-head text-capitilize">
                                    Emotive headline for about <span className="text-color-theme">
                                        the produc</span>t here
                                </h2>
                                <p className="helping-info-text">
                                    Lorem ipsum dolor sit amet consectetur
                                </p>
                                {
                                    textString.map((item, index) => {
                                        return (
                                            <>
                                                <h5 className="heading-user-account">
                                                    {item.title}
                                                </h5>
                                                <p className="mb-2 paragraph-text-content">
                                                    {item.text1}
                                                    <br />
                                                    <br />
                                                    {item.text2}
                                                </p>
                                                <div className="user-button-container mt-4">
                                                    <Button className='btn-primary-content-theme'>login to your account
                                                        <span className="arrow-hand-logo">
                                                            <img src={Images.ArrowHand} />
                                                        </span>
                                                    </Button>
                                                    <p className="helping-info-text">
                                                        <img src={Images.Lock} className="protect-lock-icon" /> your information save with us
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }

                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
            <section className="emotive-headline-benefits">
                <EmotiveComponent />
            </section>
            <section className="home-banner-content-section">
                <PlatformComponent />
            </section>
            <section className="emotive-headline-benefits portfolio-headline-benefits">
                <Portfolio />
            </section>
            <section className="home-banner-content-section">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-capitilize">
                        <span className="text-color-theme">
                            Download Now
                        </span>
                    </h2>
                    <ul className="order-download-store">
                        <li className="order-store-listing">
                            <span className="store-app-manager">
                                <img src={Images.AppStore} />
                            </span>
                        </li>
                        <li className="order-store-listing">
                            <span className="store-app-manager">
                                <img src={Images.Playstore} />
                            </span>
                        </li>

                    </ul>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default Home;