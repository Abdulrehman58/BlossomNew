import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import { inputLists } from "../../constant/dummyData";
import Footer from "../../components/footer/Footer";
import InputContact from "../../commonComponents";


function ContactUs() {
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        Contact Us
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Enter the details for contact us!
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">
                        {
                            inputLists.map((item, index) => {
                                return (
                                    <>
                                        <InputContact type={item.type} placeholder={item.placeholder}
                                            icon={item.icon} />
                                    </>
                                );
                            })
                        }
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Control className="form-control-text-content" placeholder="your message" as="textarea" rows={5} />
                        </Form.Group>
                        <div className="text-center mt-4">
                            <Button variant="primary" className="form-control-btn-primary"  >
                                Submit your message
                            </Button>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default ContactUs;