import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import Images from "../../constant/images";
import { featuredDetailCard, imagePickerProfile, imageProfilePost, notificationList, setupInput } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import "./home.css";
import { useNavigate } from "react-router-dom";
import ImagePicker from "../../commonComponents/imagePicker";
import InputContact from "../../commonComponents";
import { Screens } from "../../constant/routes";
import TagsComponent from "../../commonComponents/tagsInput";
import BlossomKindHeader from "../../components/header/BlossomKind";


function CreatePost() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                   <BlossomKindHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-about-kind-heading">
                            <h5 className="user-account-title-theme text-uppercase">
                                donation campaign
                            </h5>
                        </div>
                        <div className="contact-form-content-container">
                            <Form className="form-control-contact-content form-control-contact-post mt-3">
                                <div className=" mb-4">
                                    <label className="user-profile-label-form mb-3">
                                        Select Photo / Video <span className="error-input">
                                            *
                                        </span>
                                    </label>
                                    {
                                        imageProfilePost.map((item, index) => {
                                            return (
                                                <>
                                                    <ImagePicker labelContainer={item.labelContainer} spanImage={item.spanImage}
                                                        coverImage={item.coverImage} editText={item.editText}
                                                        placeholderImage={item.placeholderImage} />
                                                </>
                                            );
                                        })
                                    }

                                </div>


                                <div className="user-form-post-card-content mb-4">
                                    <Form.Group className="mb-3 mt-4" controlId="exampleForm.ControlTextarea1">
                                        <label className="user-profile-label-form mb-3">
                                            What’s in your mind <span className="error-input">
                                                *
                                            </span>
                                        </label>
                                        <Form.Control className="form-control-text-content" placeholder="Enter Text Here" as="textarea" rows={5} />
                                    </Form.Group>
                                    <div className="mt-4 mb-3">
                                        <label className="user-profile-label-form mb-3">
                                            Tags <span className="error-input">
                                                *
                                            </span>
                                        </label>
                                        <TagsComponent tagsClassName='form-control-input-content pl-0' />
                                    </div>
                                </div>
                                <div className="">
                                    <InputContact type='text' placeholder='Goal amount' />
                                </div>

                                <div className="text-right mt-5">
                                    <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-weight-service mt-1"
                                        onClick={() => navigate(Screens.overView)}>
                                        post
                                    </Button>
                                </div>

                            </Form>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default CreatePost;