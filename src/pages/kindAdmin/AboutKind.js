import React, { useState } from "react";
import { Button, Card, Form, Modal, Tab, Tabs } from "react-bootstrap";
import Images from "../../constant/images";
import { blossomCard, featureCard, featuredDetailCard, notificationList, servicesCards, textBlossom } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import "./home.css";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";


function AboutKind() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                    <div className="home-admin-kind-dashboard">
                        <span className="user-account-logo-bar user-account-kind-menu user-account-logo-bar-top px-1">
                            <img src={Images.Menu} className="img-fluid"
                                onClick={() => navigate(Screens.blossomAbout)} />
                        </span>
                        <Form className="home-admin-kind-navbar px-1">
                            <Form.Group className="form-control-group-container" controlId="formBasicEmail">
                                <Form.Control className=" form-control-input-search"
                                    type='text'
                                    name="search"
                                    placeholder='Search'
                                />
                                <span className=" contect-form-icon-search">
                                    <img src={Images.Search} className="img-fluid" />
                                </span>
                            </Form.Group>
                        </Form>
                        <span className="user-account-logo-bar user-account-kind-menu user-account-logo-bar-top px-1">
                            <img src={Images.userProfile} className="img-fluid"
                                onClick={() => navigate(Screens.aboutProfile)} />
                        </span>
                    </div>
                    <div className="admin-dashboard-portfolio-kind">
                        <Tabs
                            defaultActiveKey="home"
                            id="uncontrolled-tab-example"
                            className="mb-3 admin-blossom-tabs-profile"
                        >
                            <Tab eventKey="home" title="Home" className="admin-blossom-tab-link">
                                {
                                    featuredDetailCard.map((item, index) => {
                                        return (
                                            <>
                                                <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer">
                                                    <div className="blossom-view-content-card-feature" >
                                                        <div className="create-account-admin-profile px-0 py-0"
                                                            onClick={() => navigate(item.link)}>
                                                            <span className="user-featured-logo mt-0">
                                                                <img src={item.attach} className="img-fluid" />
                                                            </span>
                                                            <div className="user-account-username ">
                                                                <h6 className="text-user-featured-title">
                                                                    {item.title}
                                                                </h6>
                                                                <p className="text-user-featured-description text-left">
                                                                    {item.text}
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="dropdown">
                                                            <span className="user-account-logo-bar user-account-logo-bar-dots user-account-logo-bar-top user-account-logo-bar-admin">
                                                                <img src={item.arrow} className="img-fluid img-fluid-account-user dropbtn" />
                                                            </span>
                                                            <div class="dropdown-content">
                                                                <p>Share</p>
                                                                <p>Report</p>
                                                                <p>Block</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="mt-3">
                                                        <img src={item.vector} className="img-fluid" />
                                                    </div>
                                                    <div className="user-account-username mt-3">
                                                        <p className="text-featured-description text-left">
                                                            {item.paragraph}
                                                        </p>
                                                        <h6 className="text-username-featured-title mt-2">
                                                            {item.bottomTitle}
                                                        </h6>
                                                    </div>
                                                </div>
                                            </>
                                        )
                                    })
                                }
                            </Tab>
                            <Tab eventKey="services" title="Services" className="admin-blossom-tab-link">
                                <div className="services-tab-card-container">
                                    {
                                        servicesCards.map((item, index) => {
                                            return (
                                                <>
                                                    <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer mb-4">
                                                        <div className="mt-3">
                                                            <img src={item.attach} className="img-fluid" />
                                                        </div>
                                                        <div className="user-account-username mt-3">
                                                            <div className=" create-account-admin-services px-0 py-0 mb-3">
                                                                <h6 className="text-username-featured-title mt-2">
                                                                    {item.title}
                                                                </h6>
                                                                <h6 className="text-username-featured-title mt-2">
                                                                    {item.title2}
                                                                </h6>
                                                            </div>
                                                            <p className="text-featured-description text-left">
                                                                {item.text}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </>
                                            )
                                        })
                                    }

                                </div>
                            </Tab>
                            <Tab eventKey="profile" title="Profile" className="admin-blossom-tab-link">
                                <div className="admin-dashboard-portfolio-kind">
                                    <div className="create-account-admin-profile pt-2">
                                        <span className="user-account-logo-bar user-account-logo-bar-dashboard">
                                            <img src={Images.UserLogo} className="img-fluid" />
                                        </span>
                                        <div className="user-account-username user-account-title-dashboard">
                                            <h3 className="user-account-title user-account-title-username">
                                                Saline Durene
                                            </h3>
                                        </div>
                                    </div>
                                    <div className="mt-4 blossom-view-group-container">

                                        {
                                            blossomCard.map((item, index) => {
                                                return (
                                                    <>
                                                        <div className="blossom-view-content-card" onClick={() => navigate(item.link)}>
                                                            <div className="create-account-admin-profile">
                                                                <span className="user-account-logo-bar user-account-logo-bar-top user-account-logo-bar-admin">
                                                                    <img src={item.attach} className="img-fluid" />
                                                                </span>
                                                                <div className="user-account-username ">
                                                                    <h5 className="user-account-title user-account-title-white">
                                                                        {item.title}
                                                                    </h5>
                                                                    <h6 className="user-account-description-kind user-account-title-white">
                                                                        {item.text}
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <span className="user-account-logo-bar user-account-logo-bar-top user-account-logo-bar-admin">
                                                                <img src={item.arrow} className="img-fluid" />
                                                            </span>
                                                        </div>
                                                    </>
                                                )
                                            })
                                        }
                                        <div className="blossom-view-content-card-portfolio">
                                            <div className="blossom-view-content-card-flex ">
                                                <div className="create-account-admin-profile px-0">
                                                    <h3 className="user-account-title user-account-title-username">
                                                        Jane Doe | Graphic Designer
                                                    </h3>
                                                </div>
                                                <h5 className="heading-card-content-admin">
                                                    edit
                                                </h5>
                                            </div>
                                            <div className="mt-2">
                                                <p className="">
                                                    {
                                                        textBlossom.map((item, index) => {
                                                            return (
                                                                <>
                                                                    {item.text}
                                                                    <br />
                                                                    <br />
                                                                    {item.text2}
                                                                    <br />
                                                                    <br />
                                                                    {item.text3}
                                                                </>
                                                            )
                                                        })
                                                    }

                                                </p>
                                            </div>
                                        </div>
                                    </div >
                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default AboutKind;