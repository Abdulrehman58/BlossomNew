import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import Images from "../../constant/images";
import { featuredDetailCard, historyListing, imagePickerProfile, imageProfilePost, notificationList, setupInput } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import "./home.css";
import { useNavigate } from "react-router-dom";
import ChartBar from "../../commonComponents/chartBar";
import { Screens } from "../../constant/routes";
import BlossomKindHeader from "../../components/header/BlossomKind";


function InsightKind() {
    const navigate = useNavigate()
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                   <BlossomKindHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="admin-about-kind-heading">
                            <h5 className="user-account-title-theme text-uppercase">
                                insight
                            </h5>
                        </div>
                        <div className="mt-3 text-center blossom-view-amount-donations blossom-content-card-feature-continer">
                            <h6 className="text-username-insight-title mt-2">
                                total balance
                            </h6>
                            <h3 className="title-username-insight-update">
                                $26,580.00
                            </h3>
                        </div>
                        <div className="mt-3 text-center blossom-view-amount-donations blossom-content-card-feature-continer">
                            <div className="user-chart-insight-container">
                                <p className="user-text-title-left">
                                    Earning graph
                                </p>
                                <div className="user-content-logo-arrows">
                                    <span className="user-bar-chart-content">
                                        <img src={Images.ArrowBg} className="img-fluid" />
                                    </span>
                                    <span className="user-bar-chart-content">
                                        <img src={Images.ArrowBgRight} className="img-fluid" />
                                    </span>
                                </div>
                            </div>
                            <ChartBar />
                        </div>
                        <div className="admin-about-kind-heading mt-4">
                            <h5 className="user-account-title-theme">
                                history
                            </h5>
                            <h6 className="user-account-link-all">
                                see all
                            </h6>
                        </div>
                        <div className="blossom-content-insight-feature-continer mt-4">
                            {
                                historyListing.map((item, index) => {
                                    return (
                                        <>
                                            <div className="blossom-view-content-card-feature mb-3" >
                                                <div className="create-account-admin-profile px-0 py-0"
                                                    onClick={() => navigate('')}>
                                                    <span className="user-featured-logo user-featured-logo-insight mb-0 mt-0">
                                                        <img src={item.image} className="img-fluid" />
                                                    </span>
                                                    <div className="user-account-username ">
                                                        <h6 className="text-user-featured-title text-capitalize">
                                                            {item.title}
                                                        </h6>
                                                        <p className="text-user-featured-description text-left">
                                                            {item.paragraph}
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="dropdown">
                                                    <h6 className="text-user-featured-title">
                                                        {item.price}
                                                    </h6>
                                                    <p className="text-user-featured-description text-left">
                                                        {item.date}
                                                    </p>
                                                </div>
                                            </div>
                                        </>
                                    );
                                })
                            }

                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>




        </>
    );
}
export default InsightKind;