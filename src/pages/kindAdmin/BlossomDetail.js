import React, { useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Images from "../../constant/images";
import { blossomCard, featureCard, featuredDetailCard, notificationList, textBlossom } from "../../constant/dummyData";
import AdminNavbar from "../../components/adminNavbar";
import AdminSidebar from "../../components/adminSidebar";
import "./home.css";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import BlossomKindHeader from "../../components/header/BlossomKind";


function BlossomDetail() {
    const navigate = useNavigate()
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <section className="home-content-section">
                <AdminNavbar />
            </section>
            <section className="home-banner-content-section home-admin-banner-content-section">
                <div className="home-admin-banner-content-left create-account-admin-section-kind">
                    <div className="create-account-section create-account-admin-section pt-5">
                        <AdminSidebar />
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-center">
                   <BlossomKindHeader />
                    <div className="admin-dashboard-portfolio-kind">
                        <div className="mt-4 blossom-view-group-container">
                            {
                                featuredDetailCard.map((item, index) => {
                                    return (
                                        <>
                                            <div className="mt-4 blossom-view-group-container blossom-content-card-feature-continer">
                                                <div className="blossom-view-content-card-feature" onClick={() => navigate(item.link)}>
                                                    <div className="create-account-admin-profile px-0 py-0">
                                                        <span className="user-featured-logo mt-0">
                                                            <img src={item.attach} className="img-fluid" />
                                                        </span>
                                                        <div className="user-account-username ">
                                                            <h6 className="text-user-featured-title">
                                                                {item.title}
                                                            </h6>
                                                            <p className="text-user-featured-description text-left">
                                                                {item.text}
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="dropdown">
                                                        <span className="user-account-logo-bar user-account-logo-bar-dots user-account-logo-bar-top user-account-logo-bar-admin">
                                                            <img src={item.arrow} className="img-fluid img-fluid-account-user dropbtn" />
                                                        </span>
                                                        <div class="dropdown-content">
                                                            <p>Share</p>
                                                            <p>Report</p>
                                                            <p>Block</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="mt-3">
                                                    <img src={item.vector} className="img-fluid" />
                                                </div>
                                                <div className="user-account-username mt-3">
                                                    <p className="text-featured-description text-left">
                                                        {item.paragraph}
                                                    </p>
                                                    <h6 className="text-username-featured-title mt-2">
                                                        {item.bottomTitle}
                                                    </h6>
                                                </div>
                                            </div>
                                        </>
                                    )
                                })
                            }
                        </div>
                        <div className="blossom-donation-detail-line mt-3">
                            <h6 className="text-username-featured-title mt-2">
                                Lorem ipsum dolor sit
                            </h6>
                            <p className="text-username-featured-title mt-2">
                                <b>
                                    $1,025 Raised of $2000 goal . 4 Donations
                                </b>
                            </p>
                        </div>

                        <div className="mt-5 user-donation-detail-form">
                            <InputGroup className="mb-3 user-donation-detail-group">
                                <Form.Control
                                    placeholder="Enter Amount"
                                    aria-label="Recipient's username"
                                    aria-describedby="basic-addon2"
                                    className="user-donation-detail-input"
                                />
                                <Button variant="outline-secondary" className="user-donation-detail-btn-primary" id="button-addon2"
                                    onClick={() => navigate(Screens.paymentMathod)}>
                                    donate
                                </Button>
                            </InputGroup>
                            <p className="text-featured-description text-left max-width-90-text mt-4">
                                Lorem ipsum dolor sit amet consectetur. Pellentesque duis faucibus leo diam leo neque amet enim cras. Vel sem sapien nec elementum. Sem amet porttitor condimentum vitae duis volutpat commodo neque natoque. Dis dui lacus id in. Facilisi egestas a elit sit quis dui tristique proin.
                                <br /><br />
                                Sed vitae feugiat diam nunc arcu risus. Dolor donec vulputate pulvinar sit at mi eget gravida. Et feugiat mattis gravida vel ultrices. Semper sit ut cursus vitae eget cursus arcu lectus. Molestie venenatis adipiscing vehicula vulputate. Mauris sodales lorem velit dui turpis arcu elementum. Semper sed accumsan scelerisque libero mi. Non vulputate rutrum lectus feugiat velit. Aliquam tempus fermentum a risus.
                            </p>
                            <ul className="px-0 mt-3">
                                <li className="user-blossom-confirmation-list">
                                    <span className="user-blossom-confirmation-logo">
                                        <img src={Images.Verified} className="img-fluid-content-bottom" />
                                        verified
                                    </span>
                                </li>
                                <li className="user-blossom-confirmation-list">
                                    <span className="user-blossom-confirmation-logo">
                                        <img src={Images.Trusted} className="img-fluid-content-bottom" />
                                        trusted
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="home-admin-banner-content-left home-admin-banner-content-right create-account-admin-section-kind">
                    <div className="create-account-admin-section create-account-admin-section-right">
                        <div className="create-account-admin-profile pt-5">
                            <span className="user-account-logo-bar">
                                <img src={Images.UserAccount} className="img-fluid" />
                            </span>
                            <div className="user-account-username">
                                <h5 className="user-account-title">
                                    hello john
                                </h5>
                                <h6 className="user-account-description-kind">
                                    my account
                                </h6>
                            </div>
                        </div>
                        <div className="mt-4 left-bar-notifications">
                            <div className="create-account-admin-profile px-0">
                                <span className="user-account-logo-bar user-account-logo-bar-bell">
                                    <img src={Images.BellIcon} className="img-fluid" />
                                </span>
                                <div className="user-account-username">
                                    <h6 className="user-account-description-kind user-account-description-kind-white">
                                        notification
                                    </h6>
                                </div>
                            </div>
                            <div className="mt-3">
                                {
                                    notificationList.map((item, index) => {
                                        return (
                                            <>
                                                <div className="user-account-list-bar">
                                                    <h6 className="user-account-description-kind-notofication">
                                                        {item.name}
                                                    </h6>
                                                    <p className="notification-paragraph-list">
                                                        {item.paragraph}
                                                    </p>
                                                </div>
                                            </>
                                        );
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </>
    );
}
export default BlossomDetail;