import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import { authLogin, inputLists } from "../../constant/dummyData";
import Footer from "../../components/footer/Footer";
import InputContact from "../../commonComponents";
import Images from "../../constant/images";
import PasswordInput from "../../commonComponents/password";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";


function Login() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        Login to your account
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Enter the details to login into your account
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">
                        {
                            authLogin.map((item, index) => {
                                return (
                                    <>
                                        <InputContact type={item.type} placeholder={item.placeholder}
                                            icon={item.icon} />
                                    </>
                                );
                            })
                        }
                        <PasswordInput />
                        <p className="admin-auth-redirect-link"
                            onClick={() => navigate(Screens.forgot)}>
                            fotgot password?
                        </p>

                        <div className="text-center">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"  >
                                Login
                            </Button>
                            <p className="admin-auth-redirect-link admin-auth-redirect-text"
                            >
                                Don’t have an account? <span className="text-uppercase-signup"
                                 onClick={() => navigate(Screens.signUp)}>SIGN UP</span>
                            </p>
                            <div className="mt-4">
                                <h4 className="heading-part-auth">
                                    or
                                </h4>
                                <ul className="px-0 mt-2">
                                    <li className="social-account-redirect">
                                        <span className="social-account-logo-icon">
                                            <img src={Images.GoogleLogin} className="img-fluid" />
                                        </span>
                                    </li>
                                    <li className="social-account-redirect">
                                        <span className="social-account-logo-icon">
                                            <img src={Images.FacebookLogin} className="img-fluid" />
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default Login;