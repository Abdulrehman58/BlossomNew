import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import { imagePickerProfile, setupInput } from "../../constant/dummyData";
import Footer from "../../components/footer/Footer";
import InputContact from "../../commonComponents";
import Images from "../../constant/images";
import { useNavigate } from "react-router-dom";
import ImagePicker from "../../commonComponents/imagePicker";
import { Screens } from "../../constant/routes";


function SetupProfile() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        Setup your profile
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Enter the details and setup profile
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">
                        <div className="text-center mb-4">
                            
                            {
                                imagePickerProfile.map((item, index) => {
                                    return (
                                        <>

                                            <ImagePicker labelContainer={item.labelContainer} spanImage={item.spanImage}
                                                editIcon={item.editIcon} profileContainer={item.profileContainer}
                                                placeholderImage={item.placeholderImage} coverImage={item.coverImage} />
                                        </>
                                    );
                                })
                            }

                        </div>
                        {
                            setupInput.map((item, index) => {
                                return (
                                    <>
                                        <InputContact type={item.type} placeholder={item.placeholder}
                                            icon={item.icon} />
                                    </>
                                );
                            })
                        }
                        <div className="mt-3 position-relative">
                            <Form.Select className="form-control-input-content" aria-label="Default select example">
                                <option>Gender</option>
                                <option value="1">Male</option>
                            </Form.Select>
                            <span className="contect-form-icon-content">
                                <img src={Images.UserEmail} className="img-fluid" />
                            </span>
                        </div>
                        <Form.Group className="mb-3 mt-4" controlId="exampleForm.ControlTextarea1">
                            <Form.Control className="form-control-text-content" placeholder="Add bio" as="textarea" rows={5} />
                        </Form.Group>

                        <div className="text-center mt-5">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"
                                onClick={() => navigate(Screens.connects)}>
                                Next
                            </Button>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default SetupProfile;