import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import { authLogin, inputLists, otpInputs } from "../../constant/dummyData";
import Footer from "../../components/footer/Footer";
import InputContact from "../../commonComponents";
import Images from "../../constant/images";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";


function OtpCode() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        enter otp
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Enter OTP We’ve Just Sent You by Email.
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">
                        <Form.Group className="form-control-group-container text-center mb-4" controlId="formBasicEmail">
                            {
                                otpInputs.map((item, index) => {
                                    return (

                                        <>

                                            <Form.Control className=" form-control-otp-code"
                                                type='text' placeholder={item.placeholder}
                                            />
                                        </>
                                    )
                                })
                            }
                        </Form.Group>


                        <div className="text-center">
                            <p className="admin-auth-redirect-link mb-4 text-center"
                            >
                                Don’t get one time code ?
                            </p>
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight form-control-btn-otp-code mt-1"
                                onClick={() => navigate(Screens.createPassword)} >
                                resend
                            </Button>

                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default OtpCode;