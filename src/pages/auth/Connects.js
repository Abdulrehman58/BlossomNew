import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import { connectApp } from "../../constant/dummyData";
import Footer from "../../components/footer/Footer";
import { useNavigate } from "react-router-dom";
import ConnectsApp from "../../components/connectsApp";
import { Screens } from "../../constant/routes";


function Connects() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        connect apps
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Connect your social accounts
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">
                        <ul className="px-0">
                            {
                                connectApp.map((item, index) => {
                                    return (
                                        <>
                                            <ConnectsApp social={item.social} name={item.name} />
                                        </>
                                    );
                                })
                            }
                        </ul>
                        <div className="text-center">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"
                                onClick={() => navigate(Screens.locations)}>
                                Next
                            </Button>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default Connects;