import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import { authLogin, authSignup, passwordInputs } from "../../constant/dummyData";
import Footer from "../../components/footer/Footer";
import InputContact from "../../commonComponents";
import Images from "../../constant/images";
import PasswordInput from "../../commonComponents/password";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";


function CreatePassword() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        Create new password
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Create new password
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">

                        {
                            passwordInputs.map((item, index) => {
                                return (
                                    <>
                                        <PasswordInput placeholder={item.placeholder} />
                                    </>
                                );
                            })
                        }


                        <div className="text-center">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"
                                onClick={() => navigate(Screens.setupProfile)}>
                                continue
                            </Button>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default CreatePassword;