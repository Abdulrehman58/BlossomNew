import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import Footer from "../../components/footer/Footer";
import Images from "../../constant/images";
import { useNavigate } from "react-router-dom";
import ClassicMap from "../../commonComponents/mapComponent";
import { Screens } from "../../constant/routes";


function Location() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        Set your location
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Enter your location
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">

                        <div className="mt-3 position-relative">
                            <Form.Select className="form-control-input-content" aria-label="Default select example">
                                <option>Country</option>
                                <option value="1">Pakistan</option>
                            </Form.Select>
                            <span className="contect-form-icon-content">
                                <img src={Images.Locations} className="img-fluid" />
                            </span>
                        </div>
                        <div className="text-center mt-4">
                            <p className=" user-paragraph-text-content contact-form-text-content">
                                Select on map
                            </p>
                        </div>
                        <div className="mt-4">
                            <ClassicMap />
                        </div>

                        <div className="text-center mt-5">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"
                                onClick={() => navigate(Screens.interests)}>
                                Next
                            </Button>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default Location;