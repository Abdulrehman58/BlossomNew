import React from "react";
import NavbarComponent from "../../components/navbar";
import { Form, Button } from "react-bootstrap";
import Footer from "../../components/footer/Footer";
import Images from "../../constant/images";
import { useNavigate } from "react-router-dom";
import ClassicMap from "../../commonComponents/mapComponent";
import CheckBox from "../../commonComponents/checkBox";


function Interests() {
    const navigate = useNavigate();
    return (
        <>

            <NavbarComponent />
            <section className="about-content-section mt-5 mb-5">
                <div className="text-center">
                    <h2 className="banner-home-title-head text-uppercase mb-0">
                        Choose blossom tools
                    </h2>
                    <p className=" user-paragraph-text-content contact-form-text-content">
                        Choose the tools below
                    </p>
                </div>

                <div className="contact-form-content-container">
                    <Form className="form-control-contact-content mt-3">
                        <CheckBox />
                        <div className="text-center mt-5">
                            <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"
                                onClick={() => navigate('')}>
                                Finish
                            </Button>
                        </div>

                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
export default Interests;