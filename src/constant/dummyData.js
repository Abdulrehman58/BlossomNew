import Images from "./images";
import { Screens } from "./routes";

export const userList = [
    {
        text: 'Lorem ipsum dolor sit amet consectetur. A adipiscing et.',
        check: Images.Check,
    },
    {
        text: 'Lorem ipsum dolor sit amet consectetur. Sed.',
        check: Images.Check,
    },
    {
        text: 'Lorem ipsum dolor sit amet consectetur. Integer.',
        check: Images.Check,
    },
]
export const socialAccount = [
    {
        logo: Images.Google
    },
    {
        logo: Images.Apple
    },
    {
        logo: Images.Paypal
    },
    {
        logo: Images.CardPayment
    },
]

export const videoRecord = [
    {
        source: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
    },
    {
        source: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
    },
    {
        source: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
    },
    {
        source: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
    },
]

export const textString = [
    {
        title: 'Lorem ipsum dolor sit amet consectetur', text1: 'The secret is the acupoint pressure pad that applies gentle targeted compression to the trigger point so the pain is turned off. Simply apply BeActive® Plus to the acupressure point on the calf just below your knee.‍BeActive® Plus is the easy way to make sciatic nerve pain go away.',
        text2: 'In fact, it works so well, the FDA Cleared BeActive® Plus as a medical device.Simply apply BeActive® Plus to the acupressure point on the calf just below your knee.‍'
    }
]

export const platformCard = [
    {
        circleImage: Images.Platform1, count: '1', title: ' Create Account',
        paragraph: ' Lorem ipsum dolor sit amet consectetur. Volutpat imperdiet dignissim sit elit.',
    },
    {
        circleImage: Images.Platform2, count: '2', title: 'upload Content ',
        paragraph: ' Lorem ipsum dolor sit amet consectetur. Volutpat imperdiet dignissim sit elit.',
    },
    {
        circleImage: Images.Platform3, count: '3', title: 'Make Money',
        paragraph: ' Lorem ipsum dolor sit amet consectetur. Volutpat imperdiet dignissim sit elit.',
    },
]

export const portfolioCards = [
    {
        stars: Images.StarsGroup, title: 'Love it', text: ' Lorem ipsum dolor sit amet consectetur. Integer lobortis enim convallis varius sagittis viverra turpis. Sapien laoreet lectus suspendisse.',
        subHeading: 'Kevin Danko', circleImage: Images.Portfolio1
    },
    {
        stars: Images.StarsGroup, title: 'Love it', text: ' Lorem ipsum dolor sit amet consectetur. Integer lobortis enim convallis varius sagittis viverra turpis. Sapien laoreet lectus suspendisse.',
        subHeading: 'Eric Cornelius', circleImage: Images.Portfolio2
    },
    {
        stars: Images.StarsGroup, title: 'Love it', text: ' Lorem ipsum dolor sit amet consectetur. Integer lobortis enim convallis varius sagittis viverra turpis. Sapien laoreet lectus suspendisse.',
        subHeading: 'Rick H', circleImage: Images.Portfolio3
    },
]

export const aboutBanner = [
    {
        banner: Images.Banner4, heading: ' Lorem ipsum dolor sit amet consec',
        textOne: 'Lorem ipsum dolor sit amet consectetur. At bibendum consequat suspendisse sed elementum feugiat turpis sodales sed. Nec tortor at egestas dui mauris pellentesque suspendisse. Montes tristique id id mollis. Massa non viverra ut id tincidunt egestas elementum quam. Pretium urna malesuada id quisque euismod. Quam justo non gravida sagittis. Amet non metus id fringilla. Congue lorem pellentesque tellus odio.',
        textTwo: 'Convallis dolor tortor vel sed mi commodo. Libero odio ipsum habitant consectetur suspendisse consectetur sed elit felis. Odio elit nisi neque eget id odio. Nibh sit at venenatis aliquet amet lacinia nisl eleifend commodo. Morbi.'
    }
]

export const aboutBannerTwo = [
    {
        banner: Images.Banner5, heading: ' Lorem ipsum dolor sit amet consec',
        textOne: 'Lorem ipsum dolor sit amet consectetur. At bibendum consequat suspendisse sed elementum feugiat turpis sodales sed. Nec tortor at egestas dui mauris pellentesque suspendisse. Montes tristique id id mollis. Massa non viverra ut id tincidunt egestas elementum quam. Pretium urna malesuada id quisque euismod. Quam justo non gravida sagittis. Amet non metus id fringilla. Congue lorem pellentesque tellus odio.',
        textTwo: 'Convallis dolor tortor vel sed mi commodo. Libero odio ipsum habitant consectetur suspendisse consectetur sed elit felis. Odio elit nisi neque eget id odio. Nibh sit at venenatis aliquet amet lacinia nisl eleifend commodo. Morbi.'
    }
]

export const inputLists = [
    {
        type: 'text', placeholder: 'Name', icon: Images.Person,
    },
    {
        type: 'email', placeholder: 'Email', icon: Images.Message,
    },
    {
        type: 'email', placeholder: 'Email Subject', icon: Images.Message,
    },
]
export const authLogin = [

    {
        type: 'email', placeholder: 'Email ID', icon: Images.Mailing,
    },

]
export const setupInput = [

    {
        type: 'text', placeholder: 'Birthday', icon: Images.Calendar,
    },

]
export const authSignup = [
    {
        type: 'text', placeholder: 'Full Name', icon: Images.UserEmail,
    },
    {
        type: 'email', placeholder: 'Email ID', icon: Images.Mailing,
    },

]
export const passwordInputs = [
    {
        placeholder: 'Password',
    },
    {
        placeholder: 'Confirm Password',
    },
]

export const connectApp = [
    {
        social: Images.InstaConnect, name: 'Instagram',
    },
    {
        social: Images.FbConnect, name: 'Facebook',
    },
    {
        social: Images.XConnect, name: 'Twitter X',
    },
    {
        social: Images.UtubeConnect, name: 'Youtube',
    },
    {
        social: Images.TickConnect, name: 'Tiktok',
    },
]

export const checkInterest = [
    {
        name: 'Blossom kind',
    },
    {
        name: 'Blossom connect',
    },
    {
        name: 'Blossom View',
    },
    {
        name: 'Blossom Mart',
    },
]

export const otpInputs = [
    {
        placeholder: '1',
    },
    {
        placeholder: '5',
    },
    {
        placeholder: '8',
    },
    {
        placeholder: '4',
    },
]

export const notificationList = [
    {
        name: 'Blossom generation', paragraph: 'You purchased a digital product.',
    },
    {
        name: 'Blossom generation', paragraph: 'You purchased a digital product.',
    },
    {
        name: 'Blossom generation', paragraph: 'You purchased a digital product.',
    },
    {
        name: 'Blossom generation', paragraph: 'You purchased a digital product.',
    },
]

export const blossomCard = [
    {
        attach: Images.Attach, title: 'Blossom kind', text: 'scroll Blossom kind Feeds',
        arrow: Images.ArrowRightCard, link: Screens.blossomKind,
    },
    {
        attach: Images.Attach, title: 'Blossom Connect', text: 'scroll Blossom Connect Feeds',
        arrow: Images.ArrowRightCard,
    },
    {
        attach: Images.Attach, title: 'Blossom View', text: 'scroll Blossom View Feeds',
        arrow: Images.ArrowRightCard,
    },
    {
        attach: Images.Attach, title: 'Blossom Mart', text: 'Check our Blossom Mart Services',
        arrow: Images.ArrowRightCard,
    },
]

export const textBlossom = [
    {
        text: "Hi there! I'm Jane Doe, a passionate graphic designer based in the vibrant city of New York. I'm all about turning creative ideas into visual masterpieces. Here's a sneak peek into my design world: Creative Enthusiast: I have an unending love for colors, shapes, and all things design. Whether it's a logo, flyer, or website, I pour my heart into every project.",
        text2: "Visual Storytelling: Design, for me, is storytelling with images. I believe in the power of visuals to convey messages, evoke emotions, and leave a lasting impact.",
        text3: "Design Palette: I thrive on versatility, from minimalist aesthetics to bold and colorful designs. I'm your go-to for a wide range of design",
    },
]

export const featureCard = [
    {
        image: Images.Featured, title: 'jane doe', description: 'digital creator',
        link: '',
    },
    {
        image: Images.Featured, title: 'jane doe', description: 'digital creator',
        link: '',
    },
    {
        image: Images.Featured, title: 'jane doe', description: 'digital creator',
        link: '',
    },
    {
        image: Images.Featured, title: 'jane doe', description: 'digital creator',
        link: '',
    },
]

export const featuredDetailCard = [
    {
        attach: Images.FeatureProfile, title: 'JANE DOE', text: '1 min ago',
        arrow: Images.NotificationDots, link: Screens.blossomDetail, vector: Images.FeatureVector,
        paragraph: "Lorem ipsum dolor sit amet consectetur. Pellen duis faucibus leo diam leo. Pellen duis faucibus leo diam leo.",
        bottomTitle: '$1,025 Raised . 4 Donations'
    },
    {
        attach: Images.FeatureProfile, title: 'JANE DOE', text: '1 min ago',
        arrow: Images.NotificationDots, link: '', vector: Images.FeatureVector2,
        paragraph: "Lorem ipsum dolor sit amet consectetur. Pellen duis faucibus leo diam leo. Pellen duis faucibus leo diam leo.",
        bottomTitle: '$1,025 Raised . 4 Donations'
    },
]

export const viewHomeCard = [
    {
        attach: Images.CardUser, title: 'JANE DOE', text: '1 min ago - 1K Viewed',
        arrow: Images.NotificationDots, link: Screens.blossomDetail, vector: Images.cardVectorView,
        comments: '22 Comments 50 view', alert1: Images.Notification1, alert2: Images.Notification2,
        alert3: Images.Notification3, alert4: Images.Notification4,
    },
    {
        attach: Images.FeatureProfile, title: 'JANE DOE', text: '1 min ago - 1K Viewed',
        arrow: Images.NotificationDots, link: '', vector: Images.cardVectorView,
        comments: '22 Comments 50 view', alert1: Images.Notification1, alert2: Images.Notification2,
        alert3: Images.Notification3, alert4: Images.Notification4,
    },
]

export const viewAboutCard = [
    {
        attach: Images.Card1View, title: 'Silver Tier', text: '$14 / month',
        paragraph: "Content creators who become members often gain access to specialized tools and resource that can help them create.and publish content more effectively. This could include features like advanced editing tools, analytics dashboards, and content scheduling options.",
        bottomTitle: 'Premium content access',
    },
    {
        attach: Images.Card2View, title: 'Gold Tier', text: '$1200 / life time',
        paragraph: "Content creators who become members often gain access to specialized tools and resource that can help them create.and publish content more effectively. This could include features like advanced editing tools, analytics dashboards, and content scheduling options.",
        bottomTitle: 'Premium content access',
    },
]

export const paymentMethod = [
    {
        name: 'Google pay', image: Images.Google,
    },
    {
        name: 'Apple pay', image: Images.ApplePayment,
    },
    {
        name: 'paypal', image: Images.PaypalPayment,
    },
    {
        name: 'Debit / credit', image: Images.CardCredit,
    },
]

export const servicesCards = [
    {
        attach: Images.Signals, title: 'schedule a call', title2: '$5',
        text: 'Lorem ipsum dolor sit amet consectetur. Nibh quis enim quam quam. Mi gravida sed suspendisse.'

    },
    {
        attach: Images.ChatUser, title: 'Workshop', title2: '$70',
        text: 'Lorem ipsum dolor sit amet consectetur. Nibh quis enim quam quam. Mi gravida sed suspendisse.'

    },

]
export const imagePickerProfile = [
    {
        labelContainer: 'circle-container', spanImage: 'overlay-profile-uploader',
        editIcon: Images.EditIcon, profileContainer: 'profile-image-picker',
        placeholderImage: "cover-image-edit-logo", coverImage: Images.ProfileUser,
    },
]

export const imageProfilePost = [
    {
        labelContainer: 'circle-container circle-container-post-image', spanImage: 'overlay-profile-uploader overlay-profile-uploader-post',
        editIcon: Images.EditIcon, profileContainer: 'profile-image-picker-post',
        coverImage: Images.CoverImage, editText: 'Upload Image / Video',
        placeholderImage: "cover-image-edit-logo",
    },
]

export const createPostService = [
    {
        labelContainer: 'circle-container circle-container-post-image bg-white', spanImage: 'overlay-profile-uploader overlay-profile-uploader-post',
        editIcon: Images.EditIcon, profileContainer: 'profile-image-picker-post',
        coverImage: Images.UploadPost, classnameText: 'user-upload-profile-text',
        editText: 'Upload Image / Video',
        placeholderImage: "cover-image-edit-logo",
    },
]
export const editMembershipProfile = [
    {
        labelContainer: 'circle-container circle-container-post-image circle-container-post-edit-member bg-white', spanImage: 'overlay-profile-uploader overlay-profile-uploader-post',
        editIcon: Images.EditIcon, profileContainer: 'profile-image-picker-post',
        coverImage: Images.Camera,
        placeholderImage: "cover-image-edit-logo cover-image-edit-logo-member",
    }
]

export const historyListing = [
    {
        image: Images.Wallet, title: 'Tip received', paragraph: '09100001933311',
        price: '$10.00', date: 'Nov,25,2030',
    },
    {
        image: Images.Wallet, title: 'Tip received', paragraph: '09100001933311',
        price: '$10.00', date: 'Nov,25,2030',
    },
    {
        image: Images.Wallet, title: 'Tip received', paragraph: '09100001933311',
        price: '$10.00', date: 'Nov,25,2030',
    },
    {
        image: Images.Wallet, title: 'Tip received', paragraph: '09100001933311',
        price: '$10.00', date: 'Nov,25,2030',
    },
    {
        image: Images.Wallet, title: 'Tip received', paragraph: '09100001933311',
        price: '$10.00', date: 'Nov,25,2030',
    },
    {
        image: Images.Wallet, title: 'Tip received', paragraph: '09100001933311',
        price: '$10.00', date: 'Nov,25,2030',
    },
]

export const feedbackReviews = [
    {
        user: Images.UserReviews, title: 'jerry', description: ' Social worker',
        star: Images.Start,
        paragraph: " Lorem ipsum dolor sit amet consectetur. Felis proin est a cursus enim. Ullamcorper varius enim elementum non nulla egestas. Non neque lectus elementum arcu. Sit sodales eget diam sed neque sit nunc mattis nunc.",
    },
    {
        user: Images.UserReviews, title: 'jerry', description: ' Social worker',
        star: Images.Start,
        paragraph: " Lorem ipsum dolor sit amet consectetur. Felis proin est a cursus enim. Ullamcorper varius enim elementum non nulla egestas. Non neque lectus elementum arcu. Sit sodales eget diam sed neque sit nunc mattis nunc.",
    },
    {
        user: Images.UserReviews, title: 'jerry', description: ' Social worker',
        star: Images.Start,
        paragraph: " Lorem ipsum dolor sit amet consectetur. Felis proin est a cursus enim. Ullamcorper varius enim elementum non nulla egestas. Non neque lectus elementum arcu. Sit sodales eget diam sed neque sit nunc mattis nunc.",
    },
    {
        user: Images.UserReviews, title: 'jerry', description: ' Social worker',
        star: Images.Start,
        paragraph: " Lorem ipsum dolor sit amet consectetur. Felis proin est a cursus enim. Ullamcorper varius enim elementum non nulla egestas. Non neque lectus elementum arcu. Sit sodales eget diam sed neque sit nunc mattis nunc.",
    },
    {
        user: Images.UserReviews, title: 'jerry', description: ' Social worker',
        star: Images.Start,
        paragraph: " Lorem ipsum dolor sit amet consectetur. Felis proin est a cursus enim. Ullamcorper varius enim elementum non nulla egestas. Non neque lectus elementum arcu. Sit sodales eget diam sed neque sit nunc mattis nunc.",
    },
]

export const timeSlots = [
    {
        time: '08:00 am',
    },
    {
        time: '09:00 am',
    },
    {
        time: '10:00 am',
    },
    {
        time: '11:00 am',
    },
    {
        time: '12:00 pm',
    },
    {
        time: '01:00 pm',
    },
    {
        time: '02:00 pm',
    },
    {
        time: '03:00 pm',
    },
    {
        time: '04:00 pm',
    },
    {
        time: '05:00 pm',
    },
    {
        time: '06:00 pm',
    },
    {
        time: '07:00 pm',
    },
    {
        time: '08:00 pm',
    },
    {
        time: '09:00 pm',
    },
    {
        time: '10:00 pm',
    },
]

export const insightsListing = [
    {
        label: 'impression 229'
    },
    {
        label: 'Like 120'
    },
    {
        label: 'Comment 23'
    },
    {
        label: 'Tip $350'
    },
]

export const productCardMart = [
    {
        image: Images.Mart1,active: Images.ActiveUser, title: 'Logo Design', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart2, active: Images.ActiveUser, title: 'UI UX Design Course', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart1,active: Images.ActiveUser, title: 'Logo Design', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart2, active: Images.ActiveUser, title: 'UI UX Design Course', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart1,active: Images.ActiveUser, title: 'Logo Design', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart2, active: Images.ActiveUser, title: 'UI UX Design Course', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart1,active: Images.ActiveUser, title: 'Logo Design', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart2, active: Images.ActiveUser, title: 'UI UX Design Course', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart1,active: Images.ActiveUser, title: 'Logo Design', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart2, active: Images.ActiveUser, title: 'UI UX Design Course', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart1,active: Images.ActiveUser, title: 'Logo Design', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
    {
        image: Images.Mart2, active: Images.ActiveUser, title: 'UI UX Design Course', price: '$25',
        star: Images.StarYellow, review: '4.7', count: '(12)',
        text: "I will create professional modern minimalist business logo design",
    },
]