export const Screens = {
    Home: "/",
    About: '/about',
    contactUs: '/contact-us',
    login: '/login',
    signUp: '/sign-up',
    setupProfile: '/setup-profile',
    connects: '/connects',
    locations: '/locations',
    interests: '/interests',
    forgot: '/forgot',
    enterOtp: '/enter-otp',
    createPassword: '/create-password',

    // Blossom Kind 

    kindHome: '/kind-home',
    blossomKind: '/blossom-kind',
    blossomDetail: '/blossom-detail',
    paymentMathod: '/payment-method',
    paymentDetail: '/payment-detail',
    blossomAbout: '/about-kind',
    aboutProfile: '/about-profile',
    createPost: '/create-post',
    overView: '/overview-kind',
    insightKind: '/insight-kind',

    // Blossom Connect

    blossomHome: '/blossom-home',
    aboutConnect: '/about-connect',
    connectSchedule: '/schedule',
    profileAbout: '/profile-about',
    aboutDetail: '/about-detail',
    createService: '/create-service',

    // Blossom View
    blossomView: '/view-home',
    blossomViewAbout: '/view-about',
    blossomViewProfile: '/view-profile',
    viewCreatePost: '/view-create-post',
    postOverView: '/post-overview',
    editMembership: '/edit-membership',

    // Blossom Mart
    martHome: '/mart-home',
    productDetail: '/product-detail',
    aboutMart: '/about-mart',
    productReview: '/product-review',
};