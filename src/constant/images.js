import Stars from '../assets/images/icons/star.png'
import Users from '../assets/images/svg-icons/group-user.svg'
import Timer from '../assets/images/svg-icons/symbols-timer.svg'
import Google from '../assets/images/svg-icons/google.svg'
import Apple from '../assets/images/svg-icons/apple.svg'
import Paypal from '../assets/images/svg-icons/paypal.svg'
import CardPayment from '../assets/images/svg-icons/card-payment.svg'
import Check from '../assets/images/icons/symbols-check.png'
import Lock from '../assets/images/icons/lock.png'
import Account from '../assets/images/account-holder.png'
import Banner1 from '../assets/images/banner-1.png'
import Banner2 from '../assets/images/banner-2.png'
import Banner3 from '../assets/images/banner-4.png'
import Circle1 from '../assets/images/circle-1.png'
import Circle2 from '../assets/images/circle-2.png'
import Circle3 from '../assets/images/circle-3.png'
import Circle4 from '../assets/images/circle-4.png'
import Circle5 from '../assets/images/circle-5.png'
import Circle6 from '../assets/images/circle-6.png'
import Card1 from '../assets/images/user-one.png'
import Card2 from '../assets/images/user-2.png'
import Card3 from '../assets/images/user-3.png'
import Platform1 from '../assets/images/platform-1.png'
import Platform2 from '../assets/images/platform-2.png'
import Platform3 from '../assets/images/platform-3.png'
import BlossomLogo from '../assets/images/svg-icons/blossom-logo.svg'
import StarsGroup from '../assets/images/svg-icons/stars-group.svg'
import Playstore from '../assets/images/svg-icons/playstore.svg'
import AppStore from '../assets/images/svg-icons/app-store.svg'
import FooterLogo from '../assets/images/svg-icons/footer-logo.svg'
import FooterFb from '../assets/images/svg-icons/fb-footer.svg'
import FooterLd from '../assets/images/svg-icons/link-footer.svg'
import FooterIn from '../assets/images/svg-icons/insta-footer.svg'
import Person from '../assets/images/svg-icons/person.svg'
import Message from '../assets/images/svg-icons/message.svg'
import Mailing from '../assets/images/svg-icons/email.svg'
import LockPassword from '../assets/images/svg-icons/lock.svg'
import EyeIcon from '../assets/images/svg-icons/eye.svg'
import FacebookLogin from '../assets/images/svg-icons/facebook-login.svg'
import GoogleLogin from '../assets/images/svg-icons/google-login.svg'
import UserEmail from '../assets/images/svg-icons/user-person.svg'
import EditIcon from '../assets/images/svg-icons/bell.svg'
import Calendar from '../assets/images/svg-icons/calendar.svg'
import InstaConnect from '../assets/images/svg-icons/connect-insta.svg'
import FbConnect from '../assets/images/svg-icons/connect-fb.svg'
import XConnect from '../assets/images/svg-icons/connect-x.svg'
import UtubeConnect from '../assets/images/svg-icons/connect-utube.svg'
import TickConnect from '../assets/images/svg-icons/connect-tictok.svg'
import Locations from '../assets/images/svg-icons/location.svg'
import ArrowLeft from '../assets/images/icons/arrow-left.png'
import ArrowRight from '../assets/images/icons/arrow-right.png'
import ArrowHand from '../assets/images/icons/arrow-hand.png'
import Portfolio1 from '../assets/images/user-circle1.png'
import Portfolio2 from '../assets/images/user-circle2.png'
import Portfolio3 from '../assets/images/user-circle3.png'
import AboutVactor from '../assets/images/about-vactor.png'
import Banner4 from '../assets/images/banner4.png'
import Banner5 from '../assets/images/banner5.png'
import BGBlossom from '../assets/images/svg-icons/bg-blossom.svg'
import Dispute from '../assets/images/svg-icons/dispute.svg'
import Privacy from '../assets/images/svg-icons/privacy.svg'
import Logout from '../assets/images/svg-icons/logout.svg'
import UserAccount from '../assets/images/svg-icons/user-account.svg'
import BellIcon from '../assets/images/svg-icons/bell-icon.svg'
import Search from '../assets/images/svg-icons/search.svg'
import Scanner from '../assets/images/svg-icons/scanner.svg'
import Share from '../assets/images/svg-icons/share.svg'
import Attach from '../assets/images/svg-icons/attach.svg'
import QrCode from '../assets/images/qr-code.png'
import ArrowRightCard from '../assets/images/svg-icons/arrow-right.svg'
import Menu from '../assets/images/svg-icons/kind-menu.svg'
import userProfile from '../assets/images/svg-icons/user-profile.svg'
import Featured from '../assets/images/svg-icons/feature-profile.svg'
import Verified from '../assets/images/svg-icons/verified.svg'
import Trusted from '../assets/images/svg-icons/trusted.svg'
import GooglePayment from '../assets/images/svg-icons/google.svg'
import ApplePayment from '../assets/images/svg-icons/apple.svg'
import PaypalPayment from '../assets/images/svg-icons/paypal.svg'
import CardCredit from '../assets/images/svg-icons/card-payment.svg'
import UserGroup from '../assets/images/svg-icons/user-three.svg'
import Dollar from '../assets/images/svg-icons/dollar.svg'
import Wallet from '../assets/images/svg-icons/wallet-icon.svg'
import Start from '../assets/images/svg-icons/start.svg'
import BlossomView from '../assets/images/svg-icons/blossom-view.svg'
import BConnect from '../assets/images/svg-icons/b-connect.svg'
import UploadPost from '../assets/images/svg-icons/upload-post.svg'
import UserLogo from '../assets/images/user-profile.png'
import FeatureProfile from '../assets/images/featured-one.png'
import FeatureVector from '../assets/images/feature-1.png'
import FeatureVector2 from '../assets/images/vector-2.png'
import NotificationDots from '../assets/images/notification.png'
import Thanks from '../assets/images/thankyou.png'
import Signals from '../assets/images/singles.png'
import ChatUser from '../assets/images/user-chat.png'
import CoverImage from '../assets/images/cover-image.png'
import ArrowBg from '../assets/images/icons/arrow-left-circle.png'
import ArrowBgRight from '../assets/images/icons/arrow-right-circle.png'
import UserReviews from '../assets/images/circle-8.png';
import CardUser from '../assets/images/card-view-user.png';
import Notification1 from '../assets/images/notification-1.png';
import Notification2 from '../assets/images/notification2.png';
import Notification3 from '../assets/images/notification3.png';
import Notification4 from '../assets/images/notification4.png';
import Card1View from '../assets/images/about-view-card.png';
import Card2View from '../assets/images/about-view-card2.png';
import cardVectorView from '../assets/images/view-card.png';
import HeartBlue from '../assets/images/svg-icons/heart-blue.svg';
import MessageDots from '../assets/images/svg-icons/message-gots.svg';
import Camera from '../assets/images/svg-icons/camera.svg';
import ProfileUser from '../assets/images/svg-icons/profile-user.svg';
import MartLogo from '../assets/images/svg-icons/blossom-mart.svg';
import StarYellow from '../assets/images/svg-icons/star-yellow.svg';
import Mart1 from '../assets/images/mart-card1.png';
import Mart2 from '../assets/images/mart-2.png';
import MartDetail from '../assets/images/mart-detail.png';
import ActiveUser from '../assets/images/user-active.png';
import King from '../assets/images/king-lion.png';



const Images = {
    Stars,
    Users,
    Timer,
    Check,
    Lock,
    Account,
    Google,
    Apple,
    Paypal,
    CardPayment,
    Banner1,
    Banner2,
    ArrowLeft,
    ArrowRight,
    Banner3,
    BlossomLogo,
    Circle1, Circle2, Circle3, Circle4, Circle5, Circle6,
    Card1, Card2, Card3,
    Platform1, Platform2, Platform3,
    ArrowHand,
    StarsGroup,
    Portfolio1, Portfolio2, Portfolio3,
    Playstore, AppStore,
    FooterLogo,
    FooterFb, FooterLd, FooterIn,
    AboutVactor,
    Banner4, Banner5,
    Person, Message,
    Mailing, EyeIcon, LockPassword, FacebookLogin, GoogleLogin,
    UserEmail, EditIcon, Calendar,
    InstaConnect, UtubeConnect, XConnect, TickConnect, FbConnect,
    Locations, BGBlossom, Dispute, Privacy, Logout, UserAccount, BellIcon,
    Search, Scanner, ArrowRightCard, Share, Attach, UserLogo,
    QrCode, Menu, userProfile, Featured,
    FeatureProfile, FeatureVector, FeatureVector2, NotificationDots,
    Verified, Trusted, GooglePayment, ApplePayment, PaypalPayment, CardCredit,
    Thanks, Signals, ChatUser, UserGroup, Dollar, CoverImage,
    ArrowBg, ArrowBgRight, Wallet, UserReviews, Start,
    BConnect, UploadPost, BlossomView, CardUser, Notification1, Notification2, Notification3, Notification4,
    cardVectorView, HeartBlue, MessageDots, Card1View, Card2View, Camera,
    ProfileUser, MartLogo, StarYellow, Mart1, Mart2, MartDetail, ActiveUser,
    King,

}

export default Images;