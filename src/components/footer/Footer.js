import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Images from "../../constant/images";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";

function Footer() {
    const navigate = useNavigate();
    return (
        <>
            <footer className="footer-container-section">
                <Container>
                    <Row>
                        <Col lg={{ span: 5 }}>
                            <h4 className="footer-heading-menu">
                                About us
                            </h4>
                            <div className="mt-2 max-width-footer-left">
                                <span className="cursor-pointer" onClick={()=>navigate(Screens.Home)}>
                                    <img className="img-fluid" src={Images.FooterLogo} />
                                </span>
                                <div className="mt-3">
                                    <p className="text-white-theme">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Odio fusce ut dictumst sollicitudin lorem ac nisi.
                                        At feugiat erat massa vitae. Elit donec ut elit quis
                                        in nunc mauris. Facilisis leo aenean magna nisl sed elementum.
                                    </p>
                                    <ul className="order-download-store">
                                        <li className="order-store-listing order-footer-social">
                                            <span className="store-app-manager">
                                                <img src={Images.FooterFb} />
                                            </span>
                                        </li>
                                        <li className="order-store-listing order-footer-social">
                                            <span className="store-app-manager">
                                                <img src={Images.FooterLd} />
                                            </span>
                                        </li>
                                        <li className="order-store-listing order-footer-social">
                                            <span className="store-app-manager">
                                                <img src={Images.FooterIn} />
                                            </span>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </Col>
                        <Col lg={{ span: 4 }}>
                            <h4 className="footer-heading-menu">
                                Website
                            </h4>
                            <div className="mt-2">
                                <ul className="px-0">
                                    <li className="footer-order-list-link" onClick={()=>navigate(Screens.Home)}>
                                        Home
                                    </li>
                                    <li className="footer-order-list-link" onClick={()=>navigate(Screens.About)}>
                                        About us
                                    </li>
                                    <li className="footer-order-list-link" onClick={()=>navigate(Screens.contactUs)}>
                                        Contact us
                                    </li>
                                    <li className="footer-order-list-link">
                                        Privacy policy
                                    </li>
                                    <li className="footer-order-list-link">
                                        Terms and Conditions
                                    </li>
                                </ul>
                            </div>
                        </Col>
                        <Col lg={{ span: 3 }}>
                            <h4 className="footer-heading-menu">
                                Dowload our App
                            </h4>
                            <div className="mt-2">
                                <ul className="order-download-store">
                                    <li className="order-store-listing order-store-listing-footer">
                                        <span className="store-app-manager store-app-manager-footer">
                                            <img src={Images.AppStore} />
                                        </span>
                                    </li>
                                    <li className="order-store-listing order-store-listing-footer">
                                        <span className="store-app-manager store-app-manager-footer">
                                            <img src={Images.Playstore} />
                                        </span>
                                    </li>

                                </ul>
                                <h4 className="footer-heading-menu mt-0 mb-0">
                                    Support
                                </h4>
                                <ul className="px-0 mt-0">
                                    <li className="footer-order-list-link">
                                        Chat with us
                                    </li>
                                    <li className="footer-order-list-link">
                                        Creator
                                    </li>
                                </ul>
                            </div>

                        </Col>
                        <div className="mt-3 text-center">
                            <p className="text-white-theme text-white-theme-footer">
                                Copyrights © Blossom generation 2023
                            </p>
                        </div>
                    </Row>
                </Container>
            </footer>
        </>
    );
}
export default Footer;