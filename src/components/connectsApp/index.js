import React from "react";

function ConnectsApp(props) {
    return (
        <>
            <li className="form-control-input-content connect-app-input-content mb-4">
                <div className="connect-app-input-left">
                    <span className='social-accounts-logo'>
                        <img src={props.social} className="img-fluid" />
                    </span>
                    <p className="connect-app-input-name">
                        {props.name}
                    </p>
                </div>
                <div className="connect-app-input-right">
                    <p className="connects-app-click">
                        Connect
                    </p>
                </div>
            </li>
        </>
    );
}

export default ConnectsApp;