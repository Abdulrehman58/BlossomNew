import { Sidenav, Nav } from 'rsuite';
import DashboardIcon from '@rsuite/icons/legacy/Dashboard';
import GroupIcon from '@rsuite/icons/legacy/Group';
import SearchIcon from '@rsuite/icons/Search';
import GearIcon from '@rsuite/icons/Gear';
import WechatOutlineIcon from '@rsuite/icons/WechatOutline';
import { Screens } from '../../constant/routes';
import Logo from '../../assets/images/svg-icons/logo.svg'
import { useNavigate } from 'react-router-dom';
import Images from '../../constant/images';
import { bgBlossom } from '../../constant/svgImage';

function AdminSidebar() {
    const navigate = useNavigate()
    return (
        <>
            <Sidenav className='admin-sidebar-menubar' defaultOpenKeys={['3', '4']} style={{ height: '100%' }}>
                <Sidenav.Body className='admin-navbar-container'>
                    <Nav activeKey="1" className="first-nav">
                        <div className=''>
                            <span className='user-navbar-logo' onClick={() => navigate(Screens.Home)}>
                                <img src={Logo} />
                            </span>
                        </div>
                        <div className='admin-aoute-navigate-left'>
                            <Nav.Item className='mb-2 admin-dashboard-links' eventKey="2">
                                <span className='admin-bottom-logo-bar' style={{ marginRight: '10px' }}>
                                    <img src={Images.BGBlossom} className='img-fluid' />
                                </span>  bg main profile
                            </Nav.Item>
                            <Nav.Item className='mb-2 admin-dashboard-links' eventKey="2" icon={<WechatOutlineIcon />}>
                                chat
                            </Nav.Item>
                            <Nav.Item className='mb-2 admin-dashboard-links' eventKey="2" icon={<SearchIcon />}>
                                search
                            </Nav.Item>
                            <Nav.Item className='mb-2 admin-dashboard-links' eventKey="3" icon={<GearIcon />}>
                                settings
                            </Nav.Item>

                        </div>

                    </Nav>
                    <div className="center-space">
                    </div>
                    <Nav className="second-nav" activeKey="2">
                        <Nav.Item className='mb-2 admin-dashboard-links admin-dashboard-bottom-bar'>
                            <span className='admin-bottom-logo-bar' style={{ marginRight: '10px' }}>
                                <img src={Images.Dispute} className='img-fluid' />
                            </span>  dispute center
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links admin-dashboard-bottom-bar'>
                            <span className='admin-bottom-logo-bar' style={{ marginRight: '10px' }}>
                                <img src={Images.Privacy} className='img-fluid' />
                            </span>  privacy policy
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links admin-dashboard-bottom-bar'>
                            <span className='admin-bottom-logo-bar' style={{ marginRight: '10px' }}>
                                <img src={Images.Logout} className='img-fluid' />
                            </span>  logout
                        </Nav.Item>
                    </Nav>
                </Sidenav.Body>
            </Sidenav>
        </>
    );
}

export default AdminSidebar