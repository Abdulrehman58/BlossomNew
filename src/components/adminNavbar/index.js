import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Logo from '../../assets/images/svg-icons/logo.svg'
import { useNavigate } from 'react-router-dom';
import { Screens } from '../../constant/routes';
import 'rsuite/dist/rsuite.min.css';
import Images from '../../constant/images';

function AdminNavbar() {
    const navigate = useNavigate();
    return (
        <Navbar expand="lg" className=" bg-navbar-container bg-navbar-container-view">
            <Container >
                <Navbar.Brand>
                    <span className='user-navbar-logo' onClick={() => navigate(Screens.Home)}>
                        <img src={Logo} />
                    </span>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="navbar-collapse-content"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link className='user-navbar-content-link' onClick={() => navigate(Screens.Home)}>Home</Nav.Link>
                        <Nav.Link className='user-navbar-content-link' onClick={() => navigate(Screens.About)}>About us</Nav.Link>
                        <Nav.Link className='user-navbar-content-link' onClick={() => navigate(Screens.contactUs)}>Contact us</Nav.Link>
                        <Nav.Link className='user-navbar-content-link' onClick={() => navigate(Screens.login)}>Login</Nav.Link>
                        <Nav.Item className='mb-2 admin-dashboard-links' eventKey="2">
                            bg main profile
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links' eventKey="2" >
                            chat
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links' eventKey="2" >
                            search
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links' eventKey="3">
                            settings
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links admin-dashboard-bottom-bar'>
                            dispute center
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links admin-dashboard-bottom-bar'>
                            privacy policy
                        </Nav.Item>
                        <Nav.Item className='mb-2 admin-dashboard-links admin-dashboard-bottom-bar'>
                            logout
                        </Nav.Item>
                    </Nav>

                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default AdminNavbar;