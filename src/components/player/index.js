import React from 'react';
import { Player } from 'video-react';
import 'video-react/dist/video-react.css';

function VideoComponent(props) {
    return (
        <div className='mb-4'>
            <Player>
                <source className='user-record-detail' src={props.source} />
            </Player>
        </div>
    );
};
export default VideoComponent