import React from "react";
import { useNavigate } from "react-router-dom";
import { Screens } from "../../constant/routes";
import { Form } from "react-bootstrap";
import Images from "../../constant/images";

function BlossomKindHeader() {
    const navigate = useNavigate();
    return (
        <>
            <div className="home-admin-kind-dashboard">
                <span className="user-account-logo-bar user-account-kind-menu user-account-logo-bar-top px-1">
                    <img src={Images.Menu} className="img-fluid"
                        onClick={() => navigate(Screens.blossomAbout)} />
                </span>
                <Form className="home-admin-kind-navbar px-1">
                    <Form.Group className="form-control-group-container" controlId="formBasicEmail">
                        <Form.Control className=" form-control-input-search"
                            type='text'
                            name="search"
                            placeholder='Search'
                        />
                        <span className=" contect-form-icon-search">
                            <img src={Images.Search} className="img-fluid" />
                        </span>
                    </Form.Group>
                </Form>
                <span className="user-account-logo-bar user-account-kind-menu user-account-logo-bar-top px-1">
                    <img src={Images.userProfile} className="img-fluid"
                        onClick={() => navigate(Screens.aboutProfile)} />
                </span>
            </div>
        </>
    );
}

export default BlossomKindHeader;