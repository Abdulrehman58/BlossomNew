import React from "react";
import { Screens } from "../../constant/routes";
import { useNavigate } from "react-router-dom";
import Images from "../../constant/images";
import { Form } from "react-bootstrap";

function BlossomConnectHeader() {
    const navigate = useNavigate()
    return (
        <>
            <div className="home-admin-kind-dashboard">
                <span className="user-account-logo-bar user-account-kind-menu user-account-logo-bar-top px-1">
                    <img src={Images.BConnect} className="img-fluid"
                        onClick={() => navigate(Screens.blossomHome)} />
                </span>
                <Form className="home-admin-kind-navbar px-1">
                    <Form.Group className="form-control-group-container" controlId="formBasicEmail">
                        <Form.Control className=" form-control-input-search"
                            type='text'
                            name="search"
                            placeholder='Search'
                        />
                        <span className=" contect-form-icon-search">
                            <img src={Images.Search} className="img-fluid" />
                        </span>
                    </Form.Group>
                </Form>
                <span className="user-account-logo-bar user-account-kind-menu user-account-logo-bar-top px-1">
                    <img src={Images.userProfile} className="img-fluid"
                        onClick={() => navigate(Screens.profileAbout)} />
                </span>
            </div>
        </>
    );
}

export default BlossomConnectHeader;