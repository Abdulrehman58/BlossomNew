import React from "react";
import { Container, Row } from "react-bootstrap";
import Images from "../../constant/images";

function EmotiveComponent() {
    return (
        <>
            <Container>
                <Row>
                    <div className="text-center">
                        <h2 className="banner-home-title-head text-capitilize">
                            Emotive headline for <span className="text-color-theme">
                                Benefits</span>
                        </h2>
                        <p className=" user-paragraph-text-content">
                            Lorem ipsum dolor sit amet consectetur
                        </p>
                    </div>
                    <div className="emotive-benefits-timeline">
                        <div className="emotive-benefits-left">
                            <div className="emotive-benefits-left-profile emotive-benefits-left-profile-initail">
                                <div className="emotive-benefits-left-description">
                                    <h6 className="emotive-benefits-subheading">
                                        Lorem ispum Dolor sit
                                    </h6>
                                    <p className="emotive-benefits-paragraph">
                                        Lorem ipsum dolor sit amet consectetur. Rhoncus aliquet enim sed turpis. Id amet tristique.
                                    </p>
                                </div>
                                <span className="emotive-benfits-span-logo">
                                    <img src={Images.Circle1} />
                                </span>
                            </div>
                            <div className="emotive-benefits-left-profile emotive-benefits-left-profile-center">
                                <div className="emotive-benefits-left-description">
                                    <h6 className="emotive-benefits-subheading">
                                        Lorem ispum Dolor sit
                                    </h6>
                                    <p className="emotive-benefits-paragraph">
                                        Lorem ipsum dolor sit amet consectetur. Rhoncus aliquet enim sed turpis. Id amet tristique.
                                    </p>
                                </div>
                                <span className="emotive-benfits-span-logo">
                                    <img src={Images.Circle2} />
                                </span>
                            </div>
                            <div className="emotive-benefits-left-profile emotive-benefits-left-profile-initail">
                                <div className="emotive-benefits-left-description">
                                    <h6 className="emotive-benefits-subheading">
                                        Lorem ispum Dolor sit
                                    </h6>
                                    <p className="emotive-benefits-paragraph">
                                        Lorem ipsum dolor sit amet consectetur. Rhoncus aliquet enim sed turpis. Id amet tristique.
                                    </p>
                                </div>
                                <span className="emotive-benfits-span-logo">
                                    <img src={Images.Circle3} />
                                </span>
                            </div>
                        </div>
                        <div className="emotive-benefits-border mt-5">
                            <div className="emotive-benefits-logo">
                                <img src={Images.BlossomLogo} className="img-fluid img-fluid-logo-theme" />
                            </div>
                        </div>
                        <div className="emotive-benefits-left">
                            <div className="emotive-benefits-left-profile emotive-benefits-right-profile">
                                <span className="emotive-benfits-span-logo">
                                    <img src={Images.Circle4} />
                                </span>
                                <div className="emotive-benefits-left-description emotive-benefits-right-description">
                                    <h6 className="emotive-benefits-subheading">
                                        Lorem ispum Dolor sit
                                    </h6>
                                    <p className="emotive-benefits-paragraph">
                                        Lorem ipsum dolor sit amet consectetur. Rhoncus aliquet enim sed turpis. Id amet tristique.
                                    </p>
                                </div>

                            </div>
                            <div className="emotive-benefits-left-profile emotive-benefits-right-profile emotive-benefits-right-profile-center">
                                <span className="emotive-benfits-span-logo">
                                    <img src={Images.Circle5} />
                                </span>
                                <div className="emotive-benefits-left-description emotive-benefits-right-description">
                                    <h6 className="emotive-benefits-subheading">
                                        Lorem ispum Dolor sit
                                    </h6>
                                    <p className="emotive-benefits-paragraph">
                                        Lorem ipsum dolor sit amet consectetur. Rhoncus aliquet enim sed turpis. Id amet tristique.
                                    </p>
                                </div>

                            </div>
                            <div className="emotive-benefits-left-profile emotive-benefits-right-profile">
                                <span className="emotive-benfits-span-logo">
                                    <img src={Images.Circle6} />
                                </span>
                                <div className="emotive-benefits-left-description emotive-benefits-right-description">
                                    <h6 className="emotive-benefits-subheading">
                                        Lorem ispum Dolor sit
                                    </h6>
                                    <p className="emotive-benefits-paragraph">
                                        Lorem ipsum dolor sit amet consectetur. Rhoncus aliquet enim sed turpis. Id amet tristique.
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </Row>
            </Container>
        </>
    );
}

export default EmotiveComponent;