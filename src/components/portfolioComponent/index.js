import React from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import Images from "../../constant/images";
import { platformCard, portfolioCards } from "../../constant/dummyData";

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3
    },
    tablet: {
        breakpoint: { max: 1024, min: 576 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 575, min: 0 },
        items: 1
    }
};

function Portfolio() {
    return (
        <>
            <Container>
                <Row>
                    <div className="text-center">
                        <h2 className="banner-home-title-head text-capitilize">
                            Hear from our <span className="text-color-theme">
                                happy Users</span>!
                        </h2>
                        <p className=" user-paragraph-text-content">
                            Lorem ipsum dolor sit amet consectetur
                        </p>
                    </div>
                    <Col className="portfolio-headline-benefits " lg={{ span: 12 }}>
                        {/* <Carousel className="text-center" responsive={responsive}> */}
                        <div className="portfolio-card-carousels-section">
                            {
                                portfolioCards.map((item, index) => {
                                    return (
                                        <>
                                            <div className="hear-from-user-card">
                                                <span className="hear-from-user-stars">
                                                    <img src={item.stars} className="img-fluid" />
                                                </span>
                                                <div className="mt-4">
                                                    <h6 className="emotive-benefits-subheading emotive-theme-subheading">
                                                        <b>{item.title}</b>
                                                        <br />

                                                    </h6>
                                                    <p className="emotive-benefits-paragraph emotive-theme-subheading">
                                                        {item.text}

                                                    </p>
                                                    <h6 className="">
                                                        <b>{item.subHeading}</b>
                                                    </h6>
                                                </div>
                                                <span className="user-card-circle-logo">
                                                    <img src={item.circleImage} />
                                                </span>
                                            </div>
                                        </>
                                    );
                                })
                            }
                            </div>

                        {/* </Carousel> */}
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default Portfolio;