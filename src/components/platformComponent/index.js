import React from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import Images from "../../constant/images";
import { platformCard } from "../../constant/dummyData";

function PlatformComponent() {
    return (
        <>
            <Container>
                <Row>
                    <div className="text-center">
                        <h2 className="banner-home-title-head text-capitilize">
                            How to use <span className="text-color-theme">
                                Blossom generation</span> platform
                        </h2>
                        <p className=" user-paragraph-text-content">
                            Lorem ipsum dolor sit amet consectetur
                        </p>
                    </div>
                    {
                        platformCard.map((item, index) => {
                            return (
                                <>
                                    <Col lg={{ span: 4 }} md={{ span: 6 }} sm={{ span: 12 }}>
                                        <div className="card-user-account-platform mt-5">
                                            <div className="card-user-account-top">
                                                <span className="user-card-profile">
                                                    <img src={item.circleImage} />
                                                </span>
                                                <div className="">
                                                    <span className="user-card-account">
                                                        {item.count}
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="mt-5">
                                                <h6 className="emotive-benefits-subheading">
                                                    {item.title}
                                                </h6>
                                                <p className="emotive-benefits-paragraph">
                                                    {item.paragraph}
                                                </p>
                                            </div>
                                        </div>
                                    </Col>
                                </>
                            );
                        })
                    }

                    <div className="user-button-container mt-4">
                        <Button className='btn-primary-content-theme min-width-350'>login to your account
                            <span className="arrow-hand-logo">
                                <img src={Images.ArrowHand} />
                            </span>
                        </Button>
                        <p className="helping-info-text">
                            <img src={Images.Lock} className="protect-lock-icon" /> your information save with us
                        </p>
                    </div>

                </Row>
            </Container>
        </>
    );
}

export default PlatformComponent;