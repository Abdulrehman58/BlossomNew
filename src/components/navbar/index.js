import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Logo from '../../assets/images/svg-icons/logo.svg'
import { useNavigate } from 'react-router-dom';
import './navbar.css'
import { Screens } from '../../constant/routes';

function NavbarComponent() {
    const navigate = useNavigate();
    return (
        <Navbar expand="lg" className=" bg-navbar-container">
            <Container >
                <Navbar.Brand>
                    <span className='user-navbar-logo' onClick={()=>navigate(Screens.Home)}>
                        <img src={Logo} />
                    </span>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="navbar-collapse-content"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link className='user-navbar-content-link' onClick={()=>navigate(Screens.Home)}>Home</Nav.Link>
                        <Nav.Link className='user-navbar-content-link' onClick={()=>navigate(Screens.About)}>About us</Nav.Link>
                        <Nav.Link className='user-navbar-content-link' onClick={()=>navigate(Screens.contactUs)}>Contact us</Nav.Link>
                        <Nav.Link className='user-navbar-content-link' onClick={()=>navigate(Screens.login)}>Login</Nav.Link>
                    </Nav>
                    <Nav className="">
                        <Button className='btn-primary-content-navbar'>get started</Button>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavbarComponent;