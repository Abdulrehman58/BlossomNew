import React, {useState} from "react";
import { Form, Row, Button, Col, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Images from "../../constant/images";

function PaymentForm(props) {
    const navigate = useNavigate();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <div className="contact-form-content-container">
                <Form className="form-control-contact-content mt-3">


                    <Form.Group className="form-control-group-container mb-4" controlId="formGridAddress1">

                        <Form.Control type="text" className="form-control-input-content-payment" placeholder="card holder name" />
                    </Form.Group>

                    <Form.Group className="form-control-group-container mb-4" controlId="formGridAddress2">

                        <Form.Control type="number" className="form-control-input-content-payment" placeholder="card number" />
                    </Form.Group>
                    <Row className="mb-4">
                        <Form.Group as={Col} coform-control-group-container ntrolId="formGridEmail">

                            <Form.Control className="form-control-input-content-payment" type="number" placeholder="expiry date" />
                        </Form.Group>

                        <Form.Group as={Col} coform-control-group-container ntrolId="formGridPassword">

                            <Form.Control className="form-control-input-content-payment" type="text" placeholder="CVC" />
                        </Form.Group>
                    </Row>

                    <Form.Group className="form-control-group-container mb-4" id="formGridCheckbox">
                        <Form.Check type="checkbox" label="Save for Later" />
                    </Form.Group>

                    <Button variant="primary" className="form-control-btn-primary form-control-btn-weight mt-1"
                        onClick={handleShow}
                    >
                        Pay
                    </Button>
                </Form>
            </div>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center">
                        <span className="">
                            <img src={Images.Thanks} className="img-fluid" />
                        </span>
                    </div>
                </Modal.Body>

            </Modal>
        </>
    );
}

export default PaymentForm;