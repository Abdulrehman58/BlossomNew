import React from 'react';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Filler, Legend } from 'chart.js';
import { Line } from 'react-chartjs-2';
import faker from 'faker';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
    },
  },
};

const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'];

const data = {
  labels,
  datasets: [
    {
      fill: true,
      label: 'Month',
      data: labels.map(() => faker.datatype.number({ min: 0, max: 100 })),
      borderColor: '#2E91F7',
      backgroundColor: 'rgba(53, 162, 235, 0.5)',
    },
  ],
};

function ChartBar() {
  return <Line options={options} data={data} />;
}

export default ChartBar;
