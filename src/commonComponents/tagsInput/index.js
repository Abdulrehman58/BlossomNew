import React, { useState } from 'react';
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';

const TagsComponent = (props) => {
  const [tags, setTags] = useState([]);

  const handleChange = (tags) => {
    setTags(tags);
  };

  return <TagsInput className={props.tagsClassName} value={tags} onChange={handleChange} />;
};

export default TagsComponent;
