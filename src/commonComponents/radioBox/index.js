import React from "react";
import { paymentMethod } from "../../constant/dummyData";

function RadioBox(props) {
    return (
        <>
            <div className="">
                {
                    paymentMethod.map((item, index) => {
                        return (
                            <>
                                <div className="btn-outline-success-label-box">
                                    <label className="container-checks-box-btn container-checks-box-content">
                                        <span className="check-radio-label-title">
                                            <img src={item.image} className="img-fluid" alt={item.name} />
                                            <p className="mb-0 check-label-paragraph check-label-title-social">
                                                {item.name}
                                            </p>
                                        </span>
                                        <input type="radio" name="radioGroup" />
                                        <span className="checkmark-content-btn-outline radio-content-btn-outline"></span>
                                    </label>
                                </div>

                            </>
                        );
                    })
                }


            </div>
        </>
    );
}
export default RadioBox