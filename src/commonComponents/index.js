import React from "react";
import { Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

function InputContact(props) {
    const navigate = useNavigate();
    return (
        <>
            <Form.Group className="form-control-group-container mb-4" controlId="formBasicEmail">
                <Form.Control className="form-control-input-content" type={props.type} placeholder={props.placeholder} />
                <span className="contect-form-icon-content">
                    <img src={props.icon} className="img-fluid" />
                </span>
                <p className="admin-auth-redirect-link"
                    onClick={() => navigate(props.screens)}>
                    {props.forgot}
                </p>
            </Form.Group>

        </>
    );
}

export default InputContact;