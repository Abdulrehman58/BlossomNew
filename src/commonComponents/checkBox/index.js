import React from "react";
import { checkInterest } from "../../constant/dummyData";

function CheckBox(props) {
    return (
        <>
            <div className="">
                {
                    checkInterest.map((item, index) => {
                        return (
                            <>
                                <div className="btn-outline-success-label-box">
                                    <label class="container-checks-box-btn">
                                        <p className="mb-0 check-label-paragraph">
                                            {item.name}
                                        </p>
                                        <input type="checkbox" />
                                        <span class="checkmark-content-btn-outline"></span>
                                    </label>
                                </div>
                            </>
                        );
                    })
                }


            </div>
        </>
    );
}
export default CheckBox