import React, { useState } from "react";
import { Icon } from 'react-icons-kit';
import { eyeOff } from 'react-icons-kit/feather/eyeOff';
import { eye } from 'react-icons-kit/feather/eye'
import { Form } from "react-bootstrap";
import Images from "../../constant/images";

function PasswordInput(props) {
    const [password, setPassword] = useState("");
    const [type, setType] = useState('password');
    const [icon, setIcon] = useState(eyeOff);
    const handleToggle = () => {
        if (type === 'password') {
            setIcon(eye);
            setType('text')
        } else {
            setIcon(eyeOff)
            setType('password')
        }
    }
    return (
        <>
            <div class="mb-4 flex">
                <Form.Group className="form-control-group-container mb-4" controlId="formBasicEmail">
                    <Form.Control className="form-control-input-content"
                        type={type}
                        name="password"
                        placeholder={props.placeholder}
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        autoComplete="current-password"


                    />
                    <span className="contect-form-icon-content">
                        <img src={Images.LockPassword} className="img-fluid" />
                    </span>
                    <span class="contect-form-icon-content-hide" onClick={handleToggle}>
                        <Icon class="absolute mr-10" icon={icon} size={25} />
                    </span>
                </Form.Group>
            </div>
        </>
    );
}

export default PasswordInput;